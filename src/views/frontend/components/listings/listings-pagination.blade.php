@php
    if (!isset($extraClass)) {
        $extraClass = '';
    }
@endphp

<div class="container mx-auto text-center {{ $extraClass }}">
    {!! $data->withQueryString()->links('pagination::convert') !!}
</div>
