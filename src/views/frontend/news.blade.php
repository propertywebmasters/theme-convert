@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="latest-news">
        <div class="container mx-auto pt-12 lg:pt-16 px-8 xl:px-0 mb-8">

            @include('frontend.components.system-notifications', ['customClass' => 'mb-6'])

            <div>
                <h2 class="text-3.5xl pb-4 header-text pb-2 lg:pb-8 uppercase">{{ $header }}</h2>
            </div>

            <div class="text-xs mb-4 border-t border-b uppercase py-3">
                @include(themeViewPath('frontend.components.page-breadcrumbs'), ['navigation' => [
                   [trans('header.home') => localeUrl('/')],
                   [trans('header.latest_news') => null],
                ]])
            </div>

            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8">
                @foreach($articles as $article)
                    @include(themeViewPath('frontend.components.cards.article'), ['article' => $article])
                @endforeach
            </div>

        </div>

        @include(themeViewPath('frontend.components.listings.listings-pagination'), ['data' => $articles, 'extraClass' => 'mb-12'])

    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
