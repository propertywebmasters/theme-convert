@php
    use App\Models\SiteSetting;
    use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
    $useSettingsCache = config('platform-cache.site_settings_enabled');

    $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key'], $useSettingsCache);
    $siteEmail = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key'], $useSettingsCache);
    $siteAddress = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_ADDRESS['key'], $useSettingsCache);
@endphp

<section id="contact-form" style="background: #F5F5F5;">
    <div class="py-30">
        <div class="container mx-auto px-8 xl:px-0">
            <div class="grid grid-cols-1 lg:grid-cols-5 text-black gap-12">
                <div class="col-span-8 xl:col-span-2">
                    <div class="mb-12">
                        <h3 class="text-2xl lg:text-4.25xl lg:leading-8 -tracking-wide mb-4 capitalize-first-letter secondary-header-text make-an-enquiry-text" style="font-weight: normal;">
                            {{ trans('contact.make_an_enquiry') }}
                        </h3>
                        <p class="text-xs lg:text-sm" style="color: #696868;">
                            {{ trans('contact.get_in_contact_with_us') }}
                        </p>
                    </div>
            
                    <div class="hidden lg:block border-t border-b">
                        <div class="py-12">
                            <div>
                                <p>
                                    <a class="flex items-center" href="tel:{{ $siteTel }}">
                                        <img src="{{ themeImage('phone.svg') }}" class="svg-inject primary-text fill-current h-5" alt="phone" loading="lazy">
                                        <span class="text-sm ml-2">{{ $siteTel }}</span>
                                    </a>
                                </p>
                            </div>
                            <div class="py-4">
                                <p>
                                    <a class="flex items-center" href="mailto:{{ $siteEmail }}">
                                        <img src="{{ themeImage('email2.svg') }}" class="svg-inject primary-text fill-current h-5" alt="marker" loading="lazy">
                                        <span class="text-sm ml-2">{{ $siteEmail }}</span>
                                    </a>
                                </p>
                            </div>
                            <div>
                                <p>
                                    <div class="flex items-center">
                                        <img src="{{ themeImage('map-marker-alt.svg') }}" class="svg-inject primary-text fill-current h-5" alt="marker" loading="lazy">
                                        <span class="text-sm ml-2">{{ $siteAddress }}</span>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="col-span-8 xl:col-span-3">
                    @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6 -mt-12'])
                    <form method="post" action="{{ localeUrl('/contact') }}" class="recaptcha">
                        <div class="grid grid-cols-1 md:grid-cols-2">
                    
                            <div class="col-span-1 md:col-span-2">
                                <input class="bg-white text-black placeholder-gray-800 block w-full text-sm mb-2 focus:outline-none p-5 rounded-none" type="text" name="name" placeholder="{{ trans('contact.full_name') }} *" required>
                            </div>
                    
                            <div class="col-span-1 md:col-span-2">
                                <input class="bg-white text-black placeholder-gray-800 block w-full text-sm mb-2 focus:outline-none p-5 rounded-none" type="email" name="email" placeholder="{{ trans('contact.email_address') }} *" required>
                            </div>
                    
                            <div class="col-span-1 md:col-span-2">
                                <input class="bg-white text-black placeholder-gray-800 block w-full text-sm mb-2 focus:outline-none p-5 rounded-none" type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}">
                            </div>
                    
                            <div class="col-span-1 md:col-span-2">
                                <textarea class="bg-white text-black placeholder-gray-800 block w-full text-sm mb-2 focus:outline-none p-5 h-40 rounded-none" name="message" placeholder="{{ trans('contact.your_message') }} *" required></textarea>
                            </div>
                    
                            <div class="col-span-1 md:col-span-2">
                                @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'bg-white text-black placeholder-gray-800 block w-full text-sm mb-2 focus:outline-none p-5 rounded-none'])
                            </div>
                    
                            <div class="col-span-1 md:col-span-2">
                                <p class="text-xs modal-terms" style="color: #696868;">{!! trans('terms.modal_terms')  !!}</p>
                            </div>
                    
                            <div class="col-span-1 md:col-span-2 mt-8">
                                <button type="submit" role="button" class="relative primary-bg text-white font-bold py-4 px-6 lg:py-6 lg:px-12 text-sm uppercase cursor-pointer inline-block">
                                    {{ trans('button.enquire_now') }}
                                </button>
                            </div>
                    
                    
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
