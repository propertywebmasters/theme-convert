@push('meta_tags')
    <meta name="contact_header_feature" content="{{ (int) hasFeature(\App\Models\TenantFeature::FEATURE_CONTACT_HEADER) }}">
@endpush

@php
    use App\Http\Controllers\Frontend\AboutController;
    use App\Http\Controllers\Frontend\ContactController;
    use App\Http\Controllers\Frontend\HomeController;
    use App\Http\Controllers\Frontend\InstantValuationController;
    use App\Http\Controllers\Frontend\ValuationController;
    use App\Models\SiteSetting;
    use App\Models\TenantFeature;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;

    $visibleClass = 'navigation-bg';

    if (hasFeature(TenantFeature::FEATURE_CONTACT_HEADER)) {
        $email = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key']);
        $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key']);
        // $visibleClass .= ' top-0 z-50';
        $visibleClass .= ' top-0';
    }

    $logoPath = assetPath(app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_LOGO['key'], config('platform-cache.site_settings_enabled')));
    $controller = get_class(request()->route()->getController());
    $method = request()->route()->getActionMethod();

    // only add transparency flag on homepage
    $headerClass = $controller === HomeController::class && $method === 'index'
        ? 'home'
        : 'navigation-bg';

    $headerClass = $controller === ValuationController::class && $method === 'index'
        ? 'valuation'
        : $headerClass;

    $headerClass = $controller === InstantValuationController::class && $method === 'index'
        ? 'instant-valuation'
        : $headerClass;

    $headerClass = $controller === ContactController::class && $method === 'index'
            ? 'contact'
            : $headerClass;

    $headerClass = $controller === AboutController::class && $method === 'index'
            ? 'about'
            : $headerClass;

    $currentCurrency = currentCurrency()
@endphp

@if(env('CSS_BREAKPOINT_HELPER')) @include('frontend.components.helpers.breakpoint-helper') @endif

@if(hasFeature(\App\Models\TenantFeature::FEATURE_CONTACT_HEADER))
    @include(themeViewPath('frontend.components.contact-header'))
@endif

<header id="navigation" class="w-full fixed flex flex-col z-40 {{ $headerClass }}" data-visible-class="{{ $visibleClass }}">
    <nav>
        <div class="w-full lg:w-auto self-start sm:self-center flex flex-row sm:flex-none flex-no-wrap justify-center items-center p-4 sm:p-0 lg:hidden">
            <a id="site-logo" class="cursor-pointer lg:hidden" title="Back to homepage" href="{{ localeUrl('/') }}">
                <img src="{{ $logoPath }}" class="h-10 @if (strtolower(getFileExtensionForFilename($logoPath)) === 'svg') svg-inject @endif" alt="logo">
            </a>

            <div class="lg:hidden absolute right-8">
                <ul>
                    <li class="top-level xl:mr-8 py-2 inline-block relative text-white focus:outline-none">
                        <div id="hamburger" class="text-white text-3xl sm:text-2xl focus:outline-none lg:hidden">
                            <div class="tham tham-e-squeeze tham-w-6 bg-transparent focus:outline-none">
                                <div class="tham-box focus:outline-none">
                                    <div class="tham-inner bg-white focus:outline-none" style="background: white !important;"></div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>





        <!-- start -->
        <div id="mobile-menu" class="w-full self-end md:self-center h-full py-1 pb-4 md:py-0 lg:pb-0 hidden lg:block lg:visible h-screen">
            <ul id="navigation" class="block lg:flex text-sm xl:text-md text-white px-8 pb-4 pt-20 lg:pt-4 justify-between">
                @php
                    $half = ceil($navigation->count() / 2);
                    $chunks = $navigation->chunk($half);
                @endphp

                <div class="flex flex-col lg:flex-row items-center justify-start left-section" style="flex-grow: 1;">
                    @foreach ($chunks[0] as $nav)
                        @php
                            $anchorLabel = getLocaleAnchorLabel($nav);
                            $hasSubNav = $nav->children->count() > 0;
                            $link = $hasSubNav ? 'javascript:;' : localeUrl($nav->url);
                        @endphp
                        <li class="{{ request()->is(str_replace('/', '', $nav->url) . '*') ? 'active' : '' }} top-level mb-8 lg:mb-0 {{ $loop->last ? 'last' : 'lg:pr-4' }} block relative h-full flex items-center lg:border-b">
                            <a class="nav-text uppercase lg:text-sm {{ $nav->class }}" href="{{ $link  }}" @if(!$hasSubNav) target="{{ $nav->target }}" @endif
                            title="Select {{ \Illuminate\Support\Str::slug($anchorLabel) }}"
                            data-target="menu-{{ \Illuminate\Support\Str::slug($anchorLabel) }}">
                                {{ $anchorLabel }}
                            </a>

                            <!-- sub items -->
                            @if($nav->children->count() > 0)
                                <div
                                    class="standard-sub-nav pt-4 lg:pt-0 lg:absolute lg:left-1/2 lg:-translate-x-1/2 hidden lg:top-10 menu-{{ \Illuminate\Support\Str::slug($anchorLabel) }} z-10">
                                    <ul class="text-gray-500 w-auto text-sm xl:text-md text-gray-900 shadow">
                                        @foreach ($nav->children as $child)
                                            @php
                                                $childAnchorLabel = getLocaleAnchorLabel($child)
                                            @endphp
                                            <li class="px-4 py-2 hover:cursor-pointer border-transparent hover:primary-bg hover:text-white">
                                                <a class="nav-text whitespace-nowrap block w-full uppercase {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                                target="{{ $child->target }}">
                                                    {{ $childAnchorLabel }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <!-- end of sub items -->

                        </li>
                    @endforeach
                </div>

                <div class="center-section">
                    <a id="site-logo" class="cursor-pointer hidden lg:block" title="Back to homepage" href="{{ localeUrl('/') }}">
                        <img src="{{ $logoPath }}" class="h-12 @if (strtolower(getFileExtensionForFilename($logoPath)) === 'svg') svg-inject @endif" alt="logo">
                    </a>
                </div>

                <div class="flex flex-col lg:flex-row items-center justify-end right-section" style="flex-grow: 1;">
                    @foreach ($chunks[1] as $nav)
                        @php
                            $anchorLabel = getLocaleAnchorLabel($nav);
                            $hasSubNav = $nav->children->count() > 0;
                            $link = $hasSubNav ? 'javascript:;' : localeUrl($nav->url)
                        @endphp
                        <li class="{{ request()->is(str_replace('/', '', $nav->url) . '*') ? 'active' : '' }} top-level mb-8 lg:mb-0 {{ $loop->last ? 'last' : 'lg:pr-4' }} block relative h-full flex items-center lg:border-b">
                            <a class="nav-text uppercase lg:text-sm {{ $nav->class }}" href="{{ $link  }}" @if(!$hasSubNav) target="{{ $nav->target }}" @endif
                            title="Select {{ \Illuminate\Support\Str::slug($anchorLabel) }}"
                            data-target="menu-{{ \Illuminate\Support\Str::slug($anchorLabel) }}">
                                {{ $anchorLabel }}
                            </a>

                            <!-- sub items -->
                            @if($nav->children->count() > 0)
                                <div
                                    class="standard-sub-nav pt-4 lg:pt-0 lg:absolute lg:left-1/2 lg:-translate-x-1/2 hidden lg:top-10 menu-{{ \Illuminate\Support\Str::slug($anchorLabel) }} z-10">
                                    <ul class="text-gray-500 w-auto text-sm xl:text-md text-gray-900 shadow">
                                        @foreach ($nav->children as $child)
                                            @php
                                                $childAnchorLabel = getLocaleAnchorLabel($child)
                                            @endphp
                                            <li class="px-4 py-2 hover:cursor-pointer border-transparent hover:primary-bg hover:text-white">
                                                <a class="nav-text whitespace-nowrap block w-full uppercase {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                                target="{{ $child->target }}">
                                                    {{ $childAnchorLabel }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <!-- end of sub items -->

                        </li>
                    @endforeach

                    @if(hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
                        @if (user())
                            <li class="{{ request()->is('account*') ? 'active' : '' }} top-level mb-8 lg:mb-0 last block relative h-full flex items-center lg:border-b lg:pl-4">
                                <a class="nav-text uppercase lg:text-sm"
                                href="{{ localeUrl('/account') }}">{{ trans('auth.my_account') }}</a>
                            </li>
                        @else
                            <li class="top-level inline-block relative lg:pl-4">
                                <a class="relative modal-button cursor-pointer inline mt-6 sm:mt-0 hover-lighten lg:text-sm"
                                data-target="preauth-modal" href="javascript:">
                                    <img style="width: 15px; height: 15px;" loading="lazy" class="svg-inject text-white" src="{{ themeImage('person.svg') }}" alt="person" title="person">
                                </a>
                            </li>
                        @endif
                    @endif
                </div>

                {{-- @if(count(supportedLocales()) >= 2)
                    <li class="top-level mr-4 py-2 block relative hidden sm:hidden md:hidden lg:block xl:block">
                        <a class="hover:underline" href="javascript:" data-target="menu-locale" title="Switch to {{ app()->getLocale() }}">
                            <img src="/img/flags/{{ app()->getLocale() }}.svg" class="h-6 w-6 inline-block flag-icon" alt="{{ app()->getLocale() }}">
                        </a>
                        @if(count(supportedLocales()) > 1)
                            <div class="absolute left-1/2 -translate-x-1/2 hidden top-10 menu-locale" style="transform: translateX(-50%);">
                                <img class="m-auto" src="{{ themeImage('icons/dropmenu-icon.png') }}" alt="drop">
                                <ul class="bg-white text-gray-500 w-auto text-sm text-gray-900 shadow no-drag">
                                    @foreach(supportedLocales() as $locale)
                                        @if ($locale === app()->getLocale())
                                            @continue
                                        @endif
                                        <li class="p-1 py-2 hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                            <a class="whitespace-nowrap font-light block w-full" href="{{ switchLocaleUrl(url()->current(), $locale) }}" rel="nofollow"
                                               title="{{ countryName($locale) }}">
                                                <img src="/img/flags/{{ $locale }}.svg" class="svg-inject h-6 w-6 inline-block flag-icon" alt="{{ $locale }}">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                @endif

                @if(count($tenantCurrencies) > 1)
                    <li class="top-level mr-4 py-2 block relative hidden sm:hidden md:hidden lg:block xl:block">
                        <a class="nav-text hover:underline" href="javascript:" data-target="menu-currencies"
                           title="Switch to {{ $currentCurrency->iso }}">{{ $currentCurrency->iso }}</a>
                        <div class="absolute left-1/2 -translate-x-1/2 hidden top-10 menu-currencies" style="transform: translateX(-50%);">
                            <img class="m-auto" src="{{ themeImage('icons/dropmenu-icon.png') }}" alt="drop">
                            <ul class="bg-white text-gray-500 w-auto text-sm text-gray-900 shadow no-drag">
                                @foreach($tenantCurrencies as $currencyIso)
                                    <li class="px-4 py-2 text-gray-900  hover:bg-gray-200 hover:cursor-pointer border-transparent hover:bg-blue-500 hover:text-white">
                                        <a class="whitespace-nowrap font-light block w-full" href="{{ localeUrl('/currency/'.strtolower($currencyIso)) }}" rel="nofollow"
                                           title="{{ \App\Models\Currency::getPropertyByIso('name', $currencyIso) }} Currency">{{ strtoupper($currencyIso) }}
                                            - {!! \App\Models\Currency::getPropertyByIso('symbol', $currencyIso) !!}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endif --}}

                {{-- @if (hasFeature(\App\Models\TenantFeature::FEATURE_VALUATION_SYSTEM))
                    @php
                    $valuationDetails = getValuationUrlDetails()
                    @endphp
                    <li class="top-level mt-4 sm:mt-0 ml-0 mb-2 py-2 inline-block relative">
                        <a href="{{ $valuationDetails['url'] }}" target="{{ $valuationDetails['target'] }}"
                           class="valuation-button relative cta cta-text font-bold py-3 px-4 text-sm uppercase cursor-pointer inline">{{ trans('label.get_a_valuation') }}</a>
                    </li>
                @endif --}}

            </ul>
        </div>
        <!-- end -->









    </nav>
</header>
