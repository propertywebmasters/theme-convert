@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))
    <section id="about" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('about.hero') }}">
        <div class="py-40">
            <div class="container mx-auto px-8 xl:px-0">
                {{-- <div class="grid grid-cols-1 lg:grid-cols-8 text-black gap-12">
                    <div class="col-span-1 hidden xl:block">&nbsp;</div>

                    <div class="col-span-8 xl:col-span-6">
                        <h1 class="text-white text-5xl font-medium mx-auto py-6 header-text uppercase" style="font-weight: normal;">{!! translatableContent('about', 'about-title') !!}</h1>
                    </div>

                    <div class="col-span-1 hidden xl:block">&nbsp;</div>
                </div> --}}

                <h1 class="text-white text-5xl font-medium mx-auto py-6 secondary-header-text">{!! translatableContent('about', 'about-title') !!}</h1>
            </div>
        </div>
    </section>

    <section id="about-content" class="bg-white px-8 py-12 xl:px-0 text-black">
        <div class="container mx-auto xl:py-12">
            {{-- <div class="grid grid-cols-1 lg:grid-cols-8 text-black gap-12">
                <div class="col-span-1 hidden xl:block">&nbsp;</div>
    
                <div class="col-span-8 xl:col-span-4">
                    <div class="mb-12">
                        <h3 class="font-bold uppercase pb-6 text-2xl text-center md:text-left">{!! translatableContent('about', 'about-section-1-title') !!}</h3>
                        <p class="text-sm" style="color: #696868;">{!! translatableContent('about', 'about-section-1-text') !!}</p>
                    </div>
    
                    <div>
                        <h3 class="font-bold uppercase pb-6 text-2xl text-center md:text-left">{!! translatableContent('about', 'about-section-2-title') !!}</h3>
                        <p class="text-sm" style="color: #696868;">{!! translatableContent('about', 'about-section-2-text') !!}</p>
                    </div>
                </div>
    
                <div class="col-span-8 sm:col-span-4 xl:col-span-2">
                    <div class="grid grid-cols-3 mb-4">
                        <div class="col-span-2 tertiary-bg p-6">
                            <p class="uppercase text-white">{{ trans('generic.properties') }}</p>
                        </div>
                        <div class="col-span-1 tertiary-bg text-center w-full h-full" style="display: table;">
                            <a href="{{ localeUrl('/all-properties-for-sale') }}" class="uppercase text-sm block cta-button" style="display: table-cell; vertical-align: middle;">{{ trans('generic.view') }}</a>
                        </div>
                    </div>

                    <div class="grid grid-cols-3 mb-4">
                        <div class="col-span-2 tertiary-bg p-6">
                            <p class="uppercase text-white">{{ trans('header.latest_news') }}</p>
                        </div>
                        <div class="col-span-1 tertiary-bg text-center w-full h-full" style="display: table;">
                            <a href="{{ localeUrl('/news') }}" class="uppercase text-sm block cta-button" style="display: table-cell; vertical-align: middle;">{{ trans('generic.view') }}</a>
                        </div>
                    </div>

                    @if (!hasFeature(\App\Models\TenantFeature::FEATURE_MEET_THE_TEAM))
                        <div class="grid grid-cols-3 mb-4">
                            <div class="col-span-2 tertiary-bg p-6">
                                <p class="uppercase text-white">{{ trans('header.meet_the_team') }}</p>
                            </div>
                            <div class="col-span-1 tertiary-bg text-center w-full h-full" style="display: table;">
                                <a href="{{ localeUrl('/meet-the-team') }}" class="uppercase text-sm block cta-button" style="display: table-cell; vertical-align: middle;">{{ trans('generic.view') }}</a>
                            </div>
                        </div>
                    @endif
                    

                    <div class="grid grid-cols-3 mb-4">
                        <div class="col-span-2 tertiary-bg p-6">
                            <p class="uppercase text-white">{{ trans('generic.contact') }}</p>
                        </div>
                        <div class="col-span-1 tertiary-bg text-center w-full h-full" style="display: table;">
                            <a href="{{ localeUrl('/contact') }}" class="uppercase text-sm block cta-button" style="display: table-cell; vertical-align: middle;">{{ trans('generic.view') }}</a>
                        </div>
                    </div>
                </div>
    
                <div class="col-span-1 hidden xl:block">&nbsp;</div>
            </div> --}}
    
            <div class="grid grid-cols-1 lg:grid-cols-8 text-black gap-12">
                <div class="col-span-8 xl:col-span-5">
                    <div class="mb-12">
                        <h3 class="font-bold uppercase pb-6 text-2xl text-center md:text-left">{!! translatableContent('about', 'about-section-1-title') !!}</h3>
                        <p class="text-sm" style="color: #696868;">{!! translatableContent('about', 'about-section-1-text') !!}</p>
                    </div>
    
                    <div>
                        <h3 class="font-bold uppercase pb-6 text-2xl text-center md:text-left">{!! translatableContent('about', 'about-section-2-title') !!}</h3>
                        <p class="text-sm" style="color: #696868;">{!! translatableContent('about', 'about-section-2-text') !!}</p>
                    </div>
                </div>
    
                <div class="col-span-8 sm:col-span-4 xl:col-span-3">
                    <div class="grid grid-cols-3 mb-4">
                        <div class="col-span-2 tertiary-bg p-6">
                            <p class="uppercase text-white">{{ trans('generic.properties') }}</p>
                        </div>
                        <div class="col-span-1 tertiary-bg text-center w-full h-full" style="display: table;">
                            <a href="{{ localeUrl('/all-properties-for-sale') }}" class="uppercase text-sm block cta-button font-bold" style="display: table-cell; vertical-align: middle;">{{ trans('generic.view') }}</a>
                        </div>
                    </div>

                    <div class="grid grid-cols-3 mb-4">
                        <div class="col-span-2 tertiary-bg p-6">
                            <p class="uppercase text-white">{{ trans('header.latest_news') }}</p>
                        </div>
                        <div class="col-span-1 tertiary-bg text-center w-full h-full" style="display: table;">
                            <a href="{{ localeUrl('/news') }}" class="uppercase text-sm block cta-button font-bold" style="display: table-cell; vertical-align: middle;">{{ trans('generic.view') }}</a>
                        </div>
                    </div>

                    @if (!hasFeature(\App\Models\TenantFeature::FEATURE_MEET_THE_TEAM))
                        <div class="grid grid-cols-3 mb-4">
                            <div class="col-span-2 tertiary-bg p-6">
                                <p class="uppercase text-white">{{ trans('header.meet_the_team') }}</p>
                            </div>
                            <div class="col-span-1 tertiary-bg text-center w-full h-full" style="display: table;">
                                <a href="{{ localeUrl('/meet-the-team') }}" class="uppercase text-sm block cta-button font-bold" style="display: table-cell; vertical-align: middle;">{{ trans('generic.view') }}</a>
                            </div>
                        </div>
                    @endif
                    

                    <div class="grid grid-cols-3 mb-4">
                        <div class="col-span-2 tertiary-bg p-6">
                            <p class="uppercase text-white">{{ trans('generic.contact') }}</p>
                        </div>
                        <div class="col-span-1 tertiary-bg text-center w-full h-full" style="display: table;">
                            <a href="{{ localeUrl('/contact') }}" class="uppercase text-sm block cta-button font-bold" style="display: table-cell; vertical-align: middle;">{{ trans('generic.view') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.testimonials'))

    <section id="about-content" class="bg-white px-8 py-12 xl:px-0 text-black">
        <div class="container mx-auto xl:py-12">
            <div class="grid grid-cols-1 lg:grid-cols-8 text-black gap-12 items-center">
                {{-- <div class="col-span-1 hidden xl:block">&nbsp;</div>
    
                <div class="col-span-8 sm:col-span-4 xl:col-span-2">
                    <div>
                        <img class="w-full h-full object-cover object-center" style="aspect-ratio: 10/12;" src="{{ assetPath(translatableContent('about', 'about-main-image')) }}" alt="image">
                    </div>
                </div>

                <div class="col-span-8 xl:col-span-4">
                    <div class="text-sm">
                        <h3 class="font-bold pb-6 text-2xl uppercase">{!! translatableContent('about', 'about-main-title') !!}</h3>
                        <p class="text-sm" style="color: #696868;">{!! translatableContent('about', 'about-main-text') !!}</p>
                    </div>
                </div>
    
                <div class="col-span-1 hidden xl:block">&nbsp;</div> --}}
    
                <div class="col-span-8 sm:col-span-4 xl:col-span-3">
                    <div>
                        <img class="w-full h-full object-cover object-center" style="aspect-ratio: 10/12;" src="{{ assetPath(translatableContent('about', 'about-main-image')) }}" alt="image">
                    </div>
                </div>

                <div class="col-span-8 xl:col-span-5">
                    <div class="text-sm">
                        {{-- <h3 class="font-bold pb-6 text-2xl uppercase">{!! translatableContent('about', 'about-main-title') !!}</h3> --}}
                        <p class="text-sm" style="color: #696868;">{!! translatableContent('about', 'about-main-text') !!}</p>
                    </div>
                </div>
            </div>
    
        </div>
    </section>

    @include(themeViewPath('frontend.components.valuation'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
