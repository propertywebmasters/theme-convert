@php
    $primaryGridColsLarge = 8;
    $secondaryGridCols = 5;
    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));

    if(!isset($searchControls->tender_type) || $searchControls->tender_type !== 'both') {
        --$primaryGridColsLarge;
    }

    if(!isset($searchControls->location) || !$searchControls->location) {
        $primaryGridColsLarge -= 2;
    }

    if(!isset($searchControls->min_price) || !$searchControls->min_price) {
        --$primaryGridColsLarge;
    }
    if(!isset($searchControls->max_price) || !$searchControls->max_price) {
        --$primaryGridColsLarge;
    }

    $bedroomFilterMode = isset($searchControls->bedrooms) && $searchControls->bedrooms ? 'single' : null;
    $bedroomFilterMode = isset($searchControls->bedrooms_as_range) && $searchControls->bedrooms_as_range ? 'multi' : $bedroomFilterMode;

    if ($bedroomFilterMode === null) {
        --$primaryGridColsLarge;
    } elseif ($bedroomFilterMode === 'single') {
        --$primaryGridColsLarge;
    } elseif($bedroomFilterMode === 'multi') {
        ++$primaryGridColsLarge;
    }

    if (!$searchControls->bedrooms_as_range) {
        $primaryGridColsLarge++;
    }


    if(!isset($searchControls->property_type) || !$searchControls->property_type) {
        --$secondaryGridCols;
    }

    if(!isset($searchControls->bathrooms) || !$searchControls->bathrooms) {
        --$secondaryGridCols;
    }

    if(!isset($searchControls->reference) || !$searchControls->reference) {
        --$secondaryGridCols;
    }

    if(!isset($searchControls->name) || !$searchControls->name) {
        --$secondaryGridCols;
    }

    if(!isset($searchControls->status) || !$searchControls->status) {
        --$secondaryGridCols;
    }

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();
    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals;
@endphp

<!-- small and lower -->
<div class="block lg:hidden w-full relative">
    @include(themeViewPath('frontend.components.search-filter-mobile'))
</div>


<div class="hidden lg:block"> <!-- full search -->
    <form id="listings-search" class="listings-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded">

        <div class="grid lg:grid-cols-{{ $primaryGridColsLarge }} grid-cols-2 w-full">

            {{-- @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                <div class="bg-white p-4 cols-span-1 bg-gray-100">
                    <label for="search_type" class="text-xs">{{ trans('label.for') }}</label>
                    <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-transparent text-sm appearance-none select-carat">
                        <option value="sale" @if ($searchRequest->get('type') === 'sale') selected @endif>{{ trans('label.sale') }}</option>
                        <option value="rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === null) selected @endif>{{ trans('label.rental') }}</option>
                        @if($hasShortTermRentals)
                            <option value="short_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }}</option>
                        @endif
                        @if($hasLongTermRentals)
                            <option value="long_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                        @endif
                        @if($hasStudentRentals)
                            <option value="student_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                        @endif
                        @if($hasHolidayRentals)
                            <option value="holiday_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                        @endif
                    </select>
                </div>
            @else
                @if(isset($searchControls->tender_type))
                    @if($searchControls->tender_type === 'sale_only')
                        <input type="hidden" name="type" value="sale">
                    @elseif($searchControls->tender_type === 'rental_only')
                        <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-transparent text-sm appearance-none select-carat">
                            <option value="rental"
                                    @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === null) selected @endif>{{ trans('label.rental') }}</option>
                            @if($hasShortTermRentals)
                                <option value="short_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }}</option>
                            @endif
                            @if($hasLongTermRentals)
                                <option value="long_term_rental"
                                        @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                            @endif
                            @if($hasStudentRentals)
                                <option value="student_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                            @endif
                            @if($hasHolidayRentals)
                                <option value="holiday_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                            @endif
                        </select>
                    @endif
                @endif
            @endif --}}

            <input type="hidden" name="type" value="sale">

            @include(themeViewPath('frontend.components.location-search'), [
                'home' => true,
                'value' => $searchRequest->get('location'),
                'additionalClasses' => 'bg-transparent text-sm',
            ])

            @php
                // define default visibility for select dropdowns
                $salePriceHidden = $searchRequest->request->has('type') && $searchRequest->request->get('type') === 'rental' ? 'hidden' : '';
                $rentalPriceHidden = !$searchRequest->request->has('type') || $searchRequest->request->get('type') === 'sale' ? 'hidden' : '';
                $currentCurrency = currentCurrency()
            @endphp


            @if(isset($searchControls->min_price) && $searchControls->min_price)
                <div class="tertiary-bg text-white sale_price_filter col-span-2 lg:col-span-1 flex items-center {{ $salePriceHidden }}">
                    @include(themeViewPath('frontend.components.selects.price-select'), [
                        'mode' => 'sale',
                        'defaultRentalFrequency' => $defaultRentalFrequency,
                        'defaultCurrency' => $defaultCurrency,
                        'id' => 'search_sale_min_price',
                        'name' => 'sale_min_price',
                        'label' => trans('label.min_price'),
                        'prices' => $searchPriceRanges['sale'],
                        'initialOption' => trans('placeholder.please_select'),
                        'currentValue' => $searchRequest->request->get('min_price'),
                    ])
                </div>
            @endif

            @if(isset($searchControls->max_price) && $searchControls->max_price)
                <div class="tertiary-bg text-white sale_price_filter col-span-2 lg:col-span-1 flex items-center {{ $salePriceHidden }}">
                    @include(themeViewPath('frontend.components.selects.price-select'), [
                        'mode' => 'sale',
                        'defaultRentalFrequency' => $defaultRentalFrequency,
                        'defaultCurrency' => $defaultCurrency,
                        'id' => 'search_sale_max_price',
                        'name' => 'sale_max_price',
                        'label' => trans('label.max_price'),
                        'prices' => $searchPriceRanges['sale'],
                        'initialOption' => trans('placeholder.please_select'),
                        'currentValue' => $searchRequest->request->get('max_price'),
                    ])
                </div>
            @endif

            @if(isset($searchControls->min_price) && $searchControls->min_price)
                <div class="tertiary-bg text-white rental_price_filter p-4 col-span-2 lg:col-span-1 {{ $rentalPriceHidden }}">
                    @include(themeViewPath('frontend.components.selects.price-select'), [
                        'mode' => 'rental',
                        'defaultRentalFrequency' => $defaultRentalFrequency,
                        'defaultCurrency' => $defaultCurrency,
                        'id' => 'search_rental_min_price',
                        'name' => 'rental_min_price',
                        'label' => trans('label.min_price'),
                        'prices' => $searchPriceRanges['rental'],
                        'initialOption' => trans('placeholder.please_select'),
                        'currentValue' => $searchRequest->request->get('min_price'),
                    ])
                </div>
            @endif

            @if(isset($searchControls->max_price) && $searchControls->max_price)
                <div class="tertiary-bg text-white rental_price_filter p-4 col-span-2 lg:col-span-1 {{ $rentalPriceHidden }}">
                    @include(themeViewPath('frontend.components.selects.price-select'), [
                        'mode' => 'rental',
                        'defaultRentalFrequency' => $defaultRentalFrequency,
                        'defaultCurrency' => $defaultCurrency,
                        'id' => 'search_rental_max_price',
                        'name' => 'rental_max_price',
                        'label' => trans('label.max_price'),
                        'prices' => $searchPriceRanges['rental'],
                        'initialOption' => trans('placeholder.please_select'),
                        'currentValue' => $searchRequest->request->get('max_price'),
                    ])
                </div>
            @endif

            @if($bedroomFilterMode !== null)
                @if ($bedroomFilterMode === 'single')
                    <div class="tertiary-bg text-white col-span-2 lg:col-span-1 flex items-center">
                        {{-- <label for="search_bedrooms" class="text-xs whitespace-nowrap -ml-1">{{ trans('label.bedrooms') }}</label> --}}
                        <select id="search_bedrooms" name="bedrooms" class="border-l px-4 py-2 w-full block focus:outline-none bg-transparent text-sm appearance-none select-carat">
                            <option value="">{{ trans('label.bedrooms') }}</option>
                            @foreach($bedroomsRange as $bedrooms)
                                <option value="{{ $bedrooms }}" @if ($bedrooms === $searchRequest->get('bedrooms')) selected @endif>{{ $bedrooms }}+</option>
                            @endforeach
                        </select>
                    </div>
                @elseif($bedroomFilterMode === 'multi')
                    @php
                    if (strpos($searchRequest->get('bedrooms'), '-') !== false) {
                        $parts = explode('-', $searchRequest->get('bedrooms'));
                        $minBedsValue = isset($parts[0]) && !empty($parts[0]) ? (int) $parts[0] : null;
                        $maxBedsValue = isset($parts[1]) && !empty($parts[1]) ? (int) $parts[1] : null;
                    } else {
                        $minBedsValue = $searchRequest->get('bedrooms');
                        $maxBedsValue = $searchRequest->get('bedrooms');
                    }
                    @endphp

                    <div class="bg-gray-100 bg-white p-4 col-span-2 lg:col-span-1">
                        {{-- <label for="search_min_bedrooms" class="text-xs whitespace-nowrap -ml-1">{{ trans('label.min').' '.trans('label.bedrooms') }}</label> --}}
                        <select id="search_min_bedrooms" name="min_bedrooms" class="w-full block focus:outline-none -ml-1 bg-transparent text-sm appearance-none select-carat">
                            <option value="">{{ trans('label.min').' '.trans('label.bedrooms') }}</option>
                            @foreach($bedroomsRange as $bedrooms)
                                <option value="{{ $bedrooms }}" @if ($bedrooms === $minBedsValue) selected @endif>{{ trans('label.min') }} {{ $bedrooms }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="bg-gray-100 bg-white p-4 col-span-2 lg:col-span-1">
                        {{-- <label for="search_max_bedrooms" class="text-xs whitespace-nowrap -ml-1">{{ trans('label.max').' '.trans('label.bedrooms') }}</label> --}}
                        <select id="search_max_bedrooms" name="max_bedrooms" class="w-full block focus:outline-none -ml-1 bg-transparent text-sm appearance-none select-carat">
                            <option value="">{{ trans('label.max').' '.trans('label.bedrooms') }}</option>
                            @foreach($bedroomsRange as $bedrooms)
                                <option value="{{ $bedrooms }}" @if ($bedrooms === $maxBedsValue) selected @endif>{{ trans('label.max') }} {{ $bedrooms }}</option>
                            @endforeach
                        </select>
                    </div>

                @endif
            @endif

            <div class="tertiary-bg text-white col-span-2 lg:col-span-1 flex items-center">
                <select name="" id="listings-advanced-search-button" class="tertiary-bg block text-sm text-white w-full cursor-pointer focus:outline-none hover-lighten select-carat focus:outline-none bg-transparent text-sm appearance-none px-4 py-2 border-l">
                    <option value="">{{ trans('label.filter') }}</option>
                </select>
            </div>
            <div>
                {{-- <button type="submit" class="block text-md text-white font-bold uppercase w-full h-full focus:outline-none cursor-pointer primary-bg hover-lighten">
                    {{ trans('label.search') }} <img class="svg-inject inline-block text-white stroke-current fill-current h-4 pl-1 -mt-0.5"
                                                     src="{{ themeImage('icons/right-arrow.svg') }}">
                </button> --}}

                <button type="submit" class="text-sm font-bold uppercase w-full h-full focus:outline-none cursor-pointer cta-text cta hover-darken tracking-wider flex items-center justify-center py-6">
                    <span class="mr-2"><img style="width: 20px; height: 20px;" class="svg-inject inline-block stroke-current fill-current h-4" src="{{ themeImage('search.svg') }}" loading="lazy"></span>
                    <span class="text-sm">{{ trans('label.search') }}</span>
                </button>
            </div>
        </div>

        <div id="listings-advanced-search" class="hidden relative">
            <div class="z-10 absolute grid grid-cols-1 md:grid-cols-{{ $secondaryGridCols }} gap-4 w-full text-white p-4 pt-6 md:pt-4 tertiary-bg w-100">

                @if(isset($searchControls->property_type) && $searchControls->property_type)
                    <div>
                        <select id="property_type" name="property_type"
                                class="form-control text-white w-full p-3 focus:outline-none tertiary-bg text-gray-300 border-b border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 appearance-none select-carat">
                            <option value="">{{ trans('label.property_type') }}</option>
                            @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_HEIRARCHICAL_CATEGORIES))
                                @foreach($propertyTypes as $propertyType)
                                    @php $selected = strtolower($searchRequest->get('property_type')) === $propertyType->url_key ? 'selected' : '' @endphp
                                    <option value="{{ $propertyType->url_key }}" {{ $selected }}>{{ transPropertyType($propertyType->name) }}</option>
                                @endforeach
                            @else
                                @foreach($propertyTypeCategories as $category)
                                    @php $selected = strtolower($searchRequest->get('property_type')) === $category['category_slug'] ? 'selected' : '' @endphp
                                    <option value="{{ $category['category_slug'] }}" {{ $selected }}>{{ transPropertyType($category['category']) }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                @endif

                @if(isset($searchControls->bathrooms) && $searchControls->bathrooms)
                    <div>
                        <select id="search_bathrooms" name="bathrooms"
                                class="form-control text-white w-full p-3 focus:outline-none tertiary-bg text-gray-300 border-b border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 appearance-none select-carat">
                            <option value="">{{ trans('label.bathrooms') }}</option>
                            @foreach($bathroomsRange as $bathrooms)
                                <option value="{{ $bathrooms }}" @if ($bathrooms == $searchRequest->request->get('bathrooms')) selected @endif>Min {{ $bathrooms }}+</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                @if(isset($searchControls->reference) && $searchControls->reference)
                    <div>
                        <input style="height: 46px;" type="text" name="reference"
                               class="form-control text-white w-full p-3 focus:outline-none tertiary-bg text-gray-300 border-b border-gray-300 placeholder-gray-300"
                               placeholder="{{ trans('label.reference') }}"
                               value="{{ $searchRequest->request->get('reference') }}">
                    </div>
                @endif

                @if(isset($searchControls->name) && $searchControls->name)
                    <div>
                        <input style="height: 46px;" type="text" name="name"
                               class="form-control text-white w-full p-3 focus:outline-none tertiary-bg text-gray-300 border-b border-gray-300 placeholder-gray-300"
                               placeholder="{{ trans('label.property_name') }}">
                    </div>
                @endif

                @if(isset($searchControls->status) && $searchControls->status)
                    <div>
                        <select name="status" class="form-control text-white w-full p-3 focus:outline-none tertiary-bg text-gray-300 border-b border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 appearance-none select-carat">
                            <option value="">Status</option>{{ trans('property_status.available') }}
                            @foreach(\App\Models\Property::searchableTenderStatusList() as $tenderStatus)
                                @php
                                    $selected = strtolower($searchRequest->get('status')) === strtolower($tenderStatus) ? 'selected' : ''
                                @endphp
                                <option value="{{ $tenderStatus }}" {{ $selected }}>{{ trans('property_status.'.strtolower($tenderStatus)) }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                @if($searchableFeatures->count() > 0)
                <div class="col-span-4">
                    @include(themeViewPath('frontend.components.searchable-features'))
                </div>
                @endif

            </div>

        </div>



        @if($searchRequest->get('is_development'))
            <input type="hidden" name="is_development" value="1">
        @endif

    </form>
</div>

@include('frontend.components.modals.share-this-search')
