@if(hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
    @php
        if (!isset($splitMapMode)) {
            $splitMapMode = false;
        }
    @endphp

    <div class="sm:col-span-2 lg:col-span-3 primary-bg text-white text-center py-12 px-6 mb-8">
        {!! dynamicContent($pageContents, 'listings-alert-divider-text') !!}
        @if(user())
            <a href="{{ localeUrl('/account/alerts') }}" class="create-property-alert-button border border-white py-6 px-12 text-sm font-bold ml-4 hover:text-white block mt-2 sm:mt-0 md:mt-2 sm:inline md:block uppercase {{ $splitMapMode ? 'lg:inline-block lg:mt-4' : 'lg:inline' }}">
                <img src="{{ themeImage('icons/bell.svg') }}" class="svg-inject h-4 text-white fill-current inline mr-1"> {{ trans('search.create_property_alert') }}
            </a>
        @else
            <a href="javascript:" class="create-property-alert-button modal-button border border-white py-6 px-12 text-sm font-bold ml-4 hover:text-white block mt-2 sm:mt-0 md:mt-2 sm:inline md:block uppercase {{ $splitMapMode ? 'lg:inline-block lg:mt-4' : 'lg:inline' }}" data-target="preauth-modal">
                <img src="{{ themeImage('icons/bell.svg') }}" class="svg-inject h-4 text-white fill-current inline mr-1"> {{ trans('search.create_property_alert') }}
            </a>
        @endif
    </div>
@endif
