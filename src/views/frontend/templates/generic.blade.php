@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    @php
    use App\Models\TenantFeature;$multiColumn = hasFeature(TenantFeature::FEATURE_GENERIC_CONTACT_FORM);
    $breadcrumb['/'] = trans('header.home');

    if ($page->parent !== null) {
        $breadcrumb['/'.$page->parent->url_key] = $page->parent->title;
    }

    $breadcrumb['#'] = $page->title;

    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'))
    @endphp

    <section id="generic-page">
        <div class="container mx-auto pt-12 lg:pt-16 px-8 xl:px-0">
            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])

            <div>
                <div class="mb-4 lg:mb-0">
                    <h1 class="text-3.5xl pb-4 header-text pb-2 lg:pb-8 uppercase">{!! $page->title !!}</h1>
                    <div class="text-xs mb-4 border-t border-b uppercase py-3">
                        @include(themeViewPath('frontend.components.page-breadcrumbs'), ['navigation' => [
                           [trans('header.home') => localeUrl('/')],
                           [$page->title => null],
                        ]])
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-8">
        <div class="container mx-auto px-8 xl:px-0">
            <div>
                <div class="w-full pr-0 md:pr-8">
                    <div class="mb-16">
                        <h3 class="text-lg leading-loose tracking-tight" style="color: #242424;">{{ $page->subtitle }}</h3>
                        @if ($page->image)
                            <img src="{{ assetPath($page->image) }}" class="w-full mb-6">
                        @endif

                        <div class="generic-page-content text-sm" style="color: #242424;">{!! parseContentForShortcodes($page->content) !!}</div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    @include(themeViewPath('frontend.components.valuation'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
