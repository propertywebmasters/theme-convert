<div class="col-span-2 text-center sm:text-right">

    <form id="newsletter-signup" class="inline-block py-14 sm:pt-4 sm:pb-0 w-full" method="post" action="javascript:;" enctype="application/x-www-form-urlencoded">
        <div class="">
            <div class="leading-5 text-center lg:text-left lg:pr-6 mb-4 sm:mb-8">
                <p class="block text-left text-2xl mb-2" style="color: var(--ap-footer-link);">{{ trans('mailing-list.signup_to_our_newsletter') }}</p>
                <p class="leading-5 text-xs text-left txl:text-left block" style="color: var(--ap-footer-link);">{!! trans('mailing-list.stay_upto_date_with_our_latest_news') !!}</p>
            </div>

            <div class="grid grid-cols-3">
                {{-- <div class="mb-4 md:mb-0 col-span-2">
                    <input class="w-full block b-0 border-b-2 bg-transparent focus:outline-none h-full pr-6 text-sm" type="text" name="newsletter_name"
                           placeholder="{{ trans('contact.full_name') }}">
                </div> --}}
    
                <div class="col-span-2">
                    <input class="w-full block bg-transparent focus:outline-none h-full pr-6 text-sm py-4 px-4 rounded-none" type="email" name="newsletter_email"
                           placeholder="{{ trans('placeholder.enter_email_address') }}" style="background: #242424;">
                </div>

                <div class="col-span-1">
                    <button class="text-sm sm:text-base cta-text cta block w-full py-4 uppercase font-bold" type="submit">{{ trans('button.submit') }}</button>
                </div>
            </div>
        </div>
        @csrf
    </form>
</div>
