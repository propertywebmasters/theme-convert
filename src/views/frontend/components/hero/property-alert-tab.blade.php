{{-- @php
use App\Models\TenantFeature;
use App\Services\SocialAccount\Contracts\SocialAccountServiceInterface;
$socialCount = 0;

if (hasFeature(TenantFeature::FEATURE_SOCIAL_FIXED_TAB)) {
    $service = app()->make(SocialAccountServiceInterface::class);
    $socialAccounts = $service->get();

    if ($socialAccounts !== null) {
        $networks = ['twitter', 'linked_in', 'facebook', 'instagram'];

        foreach ($networks as $network) {
            if ($socialAccounts->$network !== null) {
                $socialCount++;
            }
        }
    }
}

$gridCols = $socialCount > 0 ? 'grid-cols-7 sm:grid-cols-5' : 'grid-cols-1';
$colspanA = $socialCount > 0 ? 'col-span-4 sm:col-span-3' : 'col-span-1';
$colspanB = $socialCount > 0 ? 'col-span-3 sm:col-span-2' : 'col-span-1'
@endphp


<div id="hero-tab" class="w-full md:w-auto text-lg lg:text-xl py-4 absolute left-0 bottom-0 z-30">
    <div class="relative" style="top: 16px;">
        <div class="grid {{ $gridCols }}">
            @if(hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
            <div class="{{ $colspanA }} primary-bg text-white">
                @if (user())
                    <a class="block h-full hover-lighten primary-bg py-4 px-6" href="{{ localeUrl('/account/alerts') }}" title="{{ trans('header.sign_up_to_property_alerts') }}">{{ trans('header.sign_up_to_property_alerts') }} <img class="svg-inject inline-block text-white stroke-current fill-current h-4 pl-2" src="{{ themeImage('icons/right-arrow.svg') }}"></a>
                @else
                    <a class="block h-full modal-button hover-lighten primary-bg py-4 text-sm sm:text-lg px-2 sm:px-6 whitespace-nowrap" href="javascript:" title="{{ trans('header.sign_up_to_property_alerts') }}" data-target="preauth-modal">{{ trans('header.sign_up_to_property_alerts') }} <img class="svg-inject inline-block text-white stroke-current fill-current h-4 pl-2 whitespace-nowrap" src="{{ themeImage('icons/right-arrow.svg') }}"></a>
                @endif
            </div>
            @endif
            @if($socialCount > 0)
            <div class="{{ $colspanB }} tertiary-bg text-white sm:px-4 sm:pt-3">
                @include(themeViewPath('frontend.components.fixed-social'))
            </div>
            @endif
        </div>
    </div>
</div> --}}
