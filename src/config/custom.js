document.addEventListener("DOMContentLoaded", function (event) {
    const btn = document.getElementById("menu");
    const menu = document.querySelector(".mobile-menu");

    // btn.addEventListener("click", () => {
    //     menu.classList.toggle("hidden");
    //     btn.classList.toggle("tham-active");
    // });
    
    const imageSlider = document.querySelector('.image-slider');

    if (imageSlider) {
        const images = document.querySelector('.slider').getAttribute('data-images').split('|'),
            mainImage = document.querySelector('#main-image'),
            loadingGif = document.querySelector('.loading-gif'),
            imageContainers = document.querySelectorAll('.image-slider-image-container'),
            prevBtn = document.querySelector('.swiper-button-prevBtn'),
            nextBtn = document.querySelector('.swiper-button-nextBtn');

        function showLoadingGif() {
            prevBtn.setAttribute('disabled', true);
            nextBtn.setAttribute('disabled', true);
            loadingGif.style.display = 'block';
        }
    
        function hideLoadingGif() {
            prevBtn.removeAttribute('disabled');
            nextBtn.removeAttribute('disabled');
            loadingGif.style.display = 'none';
        }

        function setMainImage(src) {
            if (mainImage.getAttribute('src') !== src) {
                showLoadingGif();
                mainImage.setAttribute('src', src);
            }
        }

        // move first image to end of array
        images.push(images.shift());
        
        imageContainers.forEach(function (container) {
            container.addEventListener('click', function () {
                setMainImage(this.querySelector('img').getAttribute('src'));
            });
        });

        const imageSliderSwiper = new Swiper('.image-slider', {
            slidesPerView: images.length >= 4 ? 4 : images.length,
            loop: true,
            allowTouchMove: true,
            navigation: {
                prevEl: ".swiper-button-nextBtn",
                nextEl: ".swiper-button-prevBtn",
            },
        });

        mainImage.onload = function () {
            hideLoadingGif();
        }

        imageSliderSwiper.on('slideChange', function () {
            const index = this.realIndex === 0 ? (images.length -1) : (this.realIndex - 1);
            setMainImage(images[index]);
        });
    }

    new Swiper(".featured-properties-slider", {
        slidesPerView: 1,
        spaceBetween: 4,
        loop: false,
        allowTouchMove: true,
        navigation: {
            prevEl: ".swiper-button-nextBtn",
            nextEl: ".swiper-button-prevBtn",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        breakpoints: {
            "1024": {
                slidesPerView: 2,
            },
        },
    });

    new Swiper(".listings-slider", {
        slidesPerView: 1,
        // spaceBetween: 4,
        loop: false,
        allowTouchMove: true,
        navigation: {
            prevEl: ".swiper-button-nextBtn",
            nextEl: ".swiper-button-prevBtn",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        // breakpoints: {
        //     "1024": {
        //         slidesPerView: 2,
        //     },
        // },
    });

    // new Swiper(".latest-news-slider", {
    //     slidesPerView: 1,
    //     // spaceBetween: 4,
    //     loop: false,
    //     allowTouchMove: true,
    //     navigation: {
    //         prevEl: ".latest-news-slider-swiper-button-nextBtn",
    //         nextEl: ".latest-news-slider-swiper-button-prevBtn",
    //     },
    //     // pagination: {
    //     //     el: ".latest-news-slider-swiper-pagination",
    //     //     clickable: true,
    //     // },
    //     // breakpoints: {
    //     //     "1024": {
    //     //         slidesPerView: 2,
    //     //     },
    //     // },
    // });

    const breakpoint = window.matchMedia('(min-width: 768px)');

    let latestNewsSlider, latestVideosSlider;
    
    const enableSwiper = function() {
        latestNewsSlider = new Swiper('.latest-news-slider', {
            slidesPerView: 1,
            // spaceBetween: 30,
            loop: false,
            allowTouchMove: true,
            // autoplay: {
            //     delay: 2500,
            //     disableOnInteraction: true,
            // },
            navigation: {
                prevEl: ".latest-news-slider-swiper-button-nextBtn",
                nextEl: ".latest-news-slider-swiper-button-prevBtn",
            },
        });

        latestVideosSlider = new Swiper('.latest-videos-slider', {
            slidesPerView: 1,
            loop: false,
            allowTouchMove: true,
            navigation: {
                prevEl: ".latest-videos-slider-swiper-button-nextBtn",
                nextEl: ".latest-videos-slider-swiper-button-prevBtn",
            },
        });
     };

    const breakpointChecker = function() {
        if (breakpoint.matches === true) {
            if (latestNewsSlider !== undefined) {
                latestNewsSlider.destroy(true, true);
            }

            if (latestVideosSlider !== undefined) {
                latestVideosSlider.destroy(true, true);
            }
            return;
        } else if (breakpoint.matches === false) {
           return enableSwiper();
        }
     };

     breakpoint.addEventListener('change', breakpointChecker);

     breakpointChecker();
    
    new Swiper('.testimonials-slider ', {
        slidesPerView: 1,
        loop: false,
        allowTouchMove: true,
        navigation: {
            prevEl: ".swiper-button-nextBtn",
            nextEl: ".swiper-button-prevBtn",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        autoHeight: true
    });

    SVGInject(document.querySelectorAll(".swiper-slide-duplicate img.svg-inject"));

    positionSectionUnder('body:not(.home-index):not(.news-show):not(.areaguide-index):not(.listings-index):not(.property-index) #navigation.fixed');
    resetHeight();    

    window.addEventListener('resize', function (event) {
        positionSectionUnder('body:not(.home-index):not(.news-show):not(.areaguide-index):not(.listings-index):not(.property-index) #navigation.fixed');
        resetHeight();
    });

    function positionSectionUnder(selector) {
        const fixedNav = document.querySelector(selector);
        if (fixedNav) {
            fixedNav.nextElementSibling.style.paddingTop = `${fixedNav.offsetHeight}px`;
        }
    }

    function resetHeight() {
        document.body.style.height = window.innerHeight + "px";
    }

    const contactHeaderFeature = document.querySelector('meta[name=contact_header_feature]');

    if (contactHeaderFeature && parseInt(contactHeaderFeature.getAttribute('content'))) {
        const contactHeader = document.querySelector('#contact-header'),
                homeHero = document.querySelector('#home-hero');

        if (homeHero && contactHeader) {
            homeHero.style.setProperty('height', `calc(100vh - ${contactHeader.offsetHeight}px)`);
            homeHero.style.setProperty('min-height', `calc(100vh - ${contactHeader.offsetHeight}px)`);
        }
    }

    const BREAKPOINT = document.querySelector('#contact-header') ? 1 : document.querySelector('#navigation').scrollHeight;

    scrollBody();

    window.onscroll = function (e) {
        scrollBody();
    }
    
    function scrollBody() {
        let navigation = document.querySelector('#navigation');
    
        if (navigation !== null) {
            let visibleClass = navigation.getAttribute('data-visible-class');
            visibleClass = visibleClass.split(' ');
            const body = document.querySelector('body'),
                { pageYOffset: pageOffset } = window,
                mobileMenu = document.querySelector('#mobile-menu');

            if (pageOffset >= BREAKPOINT) {
                if (navigation.classList.contains('transparent')) {
                    navigation.classList.remove('transparent');
                }
                navigation.classList.add(...visibleClass);
            } else if (!mobileMenu.classList.contains('mobile-menu-open')) {
                if (isSolidHeaderPage(body)) {
                    navigation.classList.add('transparent');
                } else {
                    const index = visibleClass.indexOf('navigation-bg');
                    if (index > -1) {
                        visibleClass.splice(index, 1);
                    }
                }

                navigation.classList.remove(...visibleClass);
            }
        }
    }
    
    function isSolidHeaderPage(body) {
        if (
            body.classList.contains('home-index') ||
            body.classList.contains('about-index') ||
            body.classList.contains('contact-index') ||
            body.classList.contains('valuation-index') ||
            body.classList.contains('instantvaluation-index') ||
            body.classList.contains('areaguide-index') ||
            body.classList.contains('listings-index') || 
            body.classList.contains('property-index') ||
            body.classList.contains('career-index') ||
            body.classList.contains('team-index') ||
            body.classList.contains('team-show') ||
            body.classList.contains('testimonial-index')) {
            return true;
        }
    
        return false;
    }

    document.querySelectorAll('.minus-button').forEach(function (minusButton) {
        minusButton.addEventListener('click', function () {
            const parent = this.closest('.mobile-footer-dropdown-container');
            
            this.classList.add('hidden');

            parent.querySelector('.plus-button').classList.remove('hidden');

            parent.querySelector('.mobile-footer-dropdown').classList.add('hidden');
        });
    });

    document.querySelectorAll('.plus-button').forEach(function (plusButton) {
        plusButton.addEventListener('click', function () {
            const parent = this.closest('.mobile-footer-dropdown-container');   

            document.querySelectorAll('.mobile-footer-dropdown').forEach(function (mobileFooterDropdown) {
                mobileFooterDropdown.classList.add('hidden');
            });

            document.querySelectorAll('.minus-button').forEach(function (minusButton) {
                minusButton.classList.add('hidden');
            });

            document.querySelectorAll('.plus-button').forEach(function (minusButton) {
                minusButton.classList.remove('hidden');
            });

            this.classList.add('hidden'); 
            parent.querySelector('.minus-button').classList.remove('hidden');
            parent.querySelector('.mobile-footer-dropdown').classList.remove('hidden');
        });
    });

    document.querySelectorAll('.expand-button').forEach(function (expandButton) {
        expandButton.addEventListener('click', function () {
            const parent = this.closest('.career-container');   

            document.querySelectorAll('.career-more-info').forEach(function (careerInfo) {
                careerInfo.classList.add('hidden');
            });

            document.querySelectorAll('.shrink-button').forEach(function (shrinkButton) {
                shrinkButton.classList.add('hidden');
            });

            document.querySelectorAll('.expand-button').forEach(function (expandButton) {
                expandButton.classList.remove('hidden');
            });

            document.querySelectorAll('.career-container').forEach(function (careerContainer) {
                careerContainer.classList.remove('active');
            });

            this.classList.add('hidden'); 
            parent.querySelector('.shrink-button').classList.remove('hidden');
            parent.querySelector('.career-more-info').classList.remove('hidden');
            parent.classList.add('active');
        });
    });

    document.querySelectorAll('.shrink-button').forEach(function (shrinkButton) {
        shrinkButton.addEventListener('click', function () {
            const parent = this.closest('.career-container');
            
            this.classList.add('hidden');

            parent.querySelector('.expand-button').classList.remove('hidden');

            parent.querySelector('.career-more-info').classList.add('hidden');

            parent.classList.remove('active');
        });
    });

    const listingsAdvancedSearchButton = document.querySelector('#listings-advanced-search-button')

    if (listingsAdvancedSearchButton) {
        listingsAdvancedSearchButton.addEventListener('mousedown', function (e) {
            e.preventDefault();
            this.blur();
            window.focus();
            document.querySelector('#listings-advanced-search').classList.toggle('hidden');
        });
    }

    const customFileInput = document.querySelector('#file_input');

    if (customFileInput) {
        document.querySelector('.custom-file-input').addEventListener('click', function () {
            document.querySelector('#file_input').click();
        });

        const fileChosen = document.querySelector('.file-chosen');

        customFileInput.addEventListener('change', function (e) {
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                fileChosen.innerHTML = fileName;
        });
    }
});
