<div class="relative z-10">
    <section id="home-hero" class="relative h-screen overflow-hidden">
        @if ($video)
            @if ($video->fallback_image_enabled)
                <div class="hidden h-screen hero-fallback-image-container">
                    @include(themeViewPath('frontend.components.hero.hero-fallback-image'))
                </div>

                <div class="hidden hero-video-container">
                    @include(themeViewPath('frontend.components.hero.hero-video'))
                </div>
            @else
                @include(themeViewPath('frontend.components.hero.hero-video'))
            @endif
        @else
            @include(themeViewPath('frontend.components.hero.hero-slideshow'))
        @endif

        @include(themeViewPath('frontend.components.hero.property-alert-tab'))

    </section>
    @include(themeViewPath('frontend.components.home-search'), ['absolutelyPosition' => true])
    {{-- @include(themeViewPath('frontend.components.hero-reviews'), ['absolutelyPosition' => true]) --}}

    <div class="absolute bottom-8 sm:bottom-4 left-1/2 transform -translate-x-1/2">
        <div class="">
            <span class="text-xs uppercase text-white uppercase cursor-pointer scroll-to" data-target="featured-properties">{{ trans('generic.see_more') }}</span>
            <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8 mx-auto" alt="arrow" loading="lazy" style="transform: rotate(90deg); width: 27.5px; height: 27.5px;">
        </div>
    </div>
</div>
