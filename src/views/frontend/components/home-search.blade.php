@php
    use App\Models\Property;use App\Models\TenantFeature;$searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));
    $absolutelyPosition = $absolutelyPosition ?? false;
    $primaryGridCols = 5;
    $secondaryGridCols = 10;

    // decrement grid col counter if we are not including the tender type control
    // primary search
    if(isset($searchControls->tender_type) && $searchControls->tender_type !== 'both') {
        --$primaryGridCols;
    }
    if(!isset($searchControls->location) || !$searchControls->location) {
        --$primaryGridCols;
    }
    // secondary/additional search
    if(!isset($searchControls->property_type) || !$searchControls->property_type) {
        --$secondaryGridCols;
    }

    $bedroomFilterMode = isset($searchControls->bedrooms) && $searchControls->bedrooms ? 'single' : null;
    $bedroomFilterMode = isset($searchControls->bedrooms_as_range) && $searchControls->bedrooms_as_range ? 'multi' : $bedroomFilterMode;

    if ($bedroomFilterMode === null) {
         $secondaryGridCols = ($secondaryGridCols - 2);
    } elseif ($bedroomFilterMode === 'single') {
        --$secondaryGridCols;
    } elseif($bedroomFilterMode === 'multi') {
        --$secondaryGridCols;
    }

    if(!isset($searchControls->bathrooms) || !$searchControls->bathrooms) {
        --$secondaryGridCols;
    }
    if(!isset($searchControls->min_price) || !$searchControls->min_price) {
        --$secondaryGridCols;
    }
    if(!isset($searchControls->max_price) || !$searchControls->max_price) {
        --$secondaryGridCols;
    }
    if(!isset($searchControls->reference) || !$searchControls->reference) {
        --$secondaryGridCols;
    }
    if(!isset($searchControls->name) || !$searchControls->name) {
        --$secondaryGridCols;
    }
    if(!isset($searchControls->status) || !$searchControls->status) {
        --$secondaryGridCols;
    }

    // DO NOT REMOVE
    // md:grid-cols-8 md:grid-cols-7 md:grid-cols-6 md:grid-cols-5
    // lg:grid-cols-8 lg:grid-cols-7 lg:grid-cols-6 lg:grid-cols-5

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();

    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals
@endphp

<div id="home-search-container" @if($absolutelyPosition) class="w-full absolute top-1/2 z-30 container left-1/2 transform -translate-x-1/2 px-8 sm:px-0 mt-8 lg:mt-0" @endif>
    <form id="home-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded" @if($absolutelyPosition) class="lg:w-1/2" @endif>
        <div class="grid grid-cols-6 md:grid-cols-3 lg:grid-cols-{{ $primaryGridCols }} w-full relative md:px-4 lg:px-0">

            {{-- @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                <div class="bg-white p-4 col-span-2 sm:col-span-1">
                    <label for="search_type" class="text-xs">{{ trans('label.for') }}</label>
                    <select id="search_type" name="type" class="w-full block focus:outline-none bg-transparent appearance-none select-carat">
                        <option value="sale">{{ trans('label.sale') }}</option>
                        <option value="rental">{{ trans('label.rental') }}</option>
                        @if($hasShortTermRentals)
                            <option value="short_term_rental">{{ trans('label.short_term_rental') }}</option>
                        @endif
                        @if($hasLongTermRentals)
                            <option value="long_term_rental">{{ trans('label.long_term_rental') }}</option>
                        @endif
                        @if($hasStudentRentals)
                            <option value="student_rental">{{ trans('label.student_rental') }}</option>
                        @endif
                        @if($hasHolidayRentals)
                            <option value="holiday_rental">{{ trans('label.holiday_rental') }}</option>
                        @endif
                    </select>
                </div>
            @elseif(isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $searchControls->tender_type === 'rental_only'))
                @if($searchControls->tender_type === 'sale_only')
                    <input id="search_type" name="type" type="hidden" value="sale">
                @elseif($searchControls->tender_type === 'rental_only')
                    <div class="bg-white p-4 col-span-2 sm:col-span-1">
                        <label for="search_type" class="text-xs">{{ trans('label.for') }}</label>
                        <select id="search_type" name="type" class="w-full block focus:outline-none bg-transparent appearance-none select-carat">
                            <option value="rental">{{ trans('label.rental') }}</option>
                            @if($hasShortTermRentals)
                                <option value="short_term_rental">{{ trans('label.short_term_rental') }}</option>
                            @endif
                            @if($hasLongTermRentals)
                                <option value="long_term_rental">{{ trans('label.long_term_rental') }}</option>
                            @endif
                        </select>
                    </div>
                @endif
            @endif --}}

            <input id="search_type" name="type" type="hidden" value="sale">

            @include(themeViewPath('frontend.components.location-search'), ['value' => $location ?? '', 'additionalClasses' => 'text-sm', 'home' => true])

            {{-- <div class="content-center z-20">
                <button type="button"
                        role="button" id="home-advanced-search-button"
                        class="block text-lg text-white font-bold uppercase w-full h-full cursor-pointer p-4 xl:p-2 focus:outline-none tertiary-bg hover-lighten tracking-wider"
                        onclick="return false;">
                    <img class="svg-inject inline-block text-white stroke-current fill-current h-5 mr-2" src="{{ themeImage('icons/filter.svg') }}"> {{ trans('label.filter') }}
                </button>
            </div> --}}
            <div class="z-20 col-span-2 md:col-span-1 lg:col-span-2 xl:col-span-1">
                <button type="submit"
                        class="text-sm font-bold uppercase w-full h-full focus:outline-none cursor-pointer hover-darken tracking-wider flex items-center justify-center py-6 cta">
                    <span class="mr-2"><img style="width: 20px; height: 20px;" class="svg-inject inline-block stroke-current fill-current h-4" src="{{ themeImage('search.svg') }}"
                                            loading="lazy"></span>
                    <span class="text-sm">{{ trans('label.search') }}</span>
                </button>
            </div>

            <div id="home-advanced-search"
                 class="z-10 absolute top-52 md:top-32 lg:top-20 gap-4 w-full text-white p-4 pt-6 md:pt-4 tertiary-bg w-100 invisible">
                <div
                    class="grid grid-cols-1 md:grid-cols-{{ $secondaryGridCols > 4 ? 4 : $secondaryGridCols }} lg:grid-cols-{{ $secondaryGridCols > 4 ? 4 : $secondaryGridCols }} xl:grid-cols-{{ $secondaryGridCols }}">

                    @if(isset($searchControls->reference) && $searchControls->reference)
                        <div>
                            <input name="reference"
                                   class="form-control w-full py-1 md:py-3 px-0 md:px-2 md:pl-0 focus:outline-none text-gray-300 border-b border-gray-300 tertiary-bg text-sm"
                                   placeholder="{{ trans('label.reference') }}">
                        </div>
                    @endif

                    @if(isset($searchControls->name) && $searchControls->name)
                        <div>
                            <input name="name"
                                   class="form-control w-full py-1 md:py-3 px-0 md:px-2 md:pl-0 focus:outline-none text-gray-300 border-b border-gray-300 tertiary-bg text-sm"
                                   placeholder="{{ trans('label.property_name') }}">
                        </div>
                    @endif

                    @if(isset($searchControls->property_type) && $searchControls->property_type)
                        <div>
                            <select id="property_type" name="property_type"
                                    class="form-control w-full py-1 md:py-3 focus:outline-none text-gray-300 border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 tertiary-bg text-sm appearance-none select-carat">
                                <option value="">{{ trans('placeholder.property_type') }}</option>
                                @if(hasFeature(TenantFeature::FEATURE_USE_HEIRARCHICAL_CATEGORIES))
                                    @foreach($propertyTypes as $propertyType)
                                        <option value="{{ $propertyType->url_key }}">{{ transPropertyType($propertyType->name) }}</option>
                                    @endforeach
                                @else
                                    @foreach($propertyTypeCategories as $category)
                                        <option value="{{ $category['category_slug'] }}">{{ transPropertyType($category['category']) }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    @endif

                    @if($bedroomFilterMode !== null)
                        @if ($bedroomFilterMode === 'single')
                            <div>
                                <select id="bedrooms" name="bedrooms"
                                        class="form-control w-full py-1 md:py-3 focus:outline-none text-gray-300 border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 tertiary-bg text-sm appearance-none select-carat">
                                    <option value="">{{ trans('placeholder.bedrooms') }}</option>
                                    @foreach($bedroomsRange as $bedroomCount)
                                        <option value="{{ $bedroomCount }}">{{ trans('label.min') }} {{ $bedroomCount }}+</option>
                                    @endforeach
                                </select>
                            </div>
                        @elseif($bedroomFilterMode === 'multi')
                            <div>
                                <select id="bedrooms" name="min_bedrooms"
                                        class="form-control w-full py-1 md:py-3 focus:outline-none text-gray-300 border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 tertiary-bg text-sm appearance-none select-carat">
                                    <option value="">{{ trans('label.min').' '.trans('placeholder.bedrooms') }}</option>
                                    @foreach($bedroomsRange as $bedroomCount)
                                        <option value="{{ $bedroomCount }}">{{ trans('label.min') }} {{ $bedroomCount }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <select id="bedrooms" name="max_bedrooms"
                                        class="form-control w-full py-1 md:py-3 focus:outline-none text-gray-300 border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 tertiary-bg text-sm appearance-none select-carat">
                                    <option value="">{{ trans('label.max').' '.trans('placeholder.bedrooms') }}</option>
                                    @foreach($bedroomsRange as $bedroomCount)
                                        <option value="{{ $bedroomCount }}">{{ trans('label.max') }} {{ $bedroomCount }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    @endif

                    @if(isset($searchControls->bathrooms) && $searchControls->bathrooms)
                        <div>
                            <select id="bathrooms" name="bathrooms"
                                    class="form-control w-full py-1 md:py-3 focus:outline-none text-gray-300 border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 tertiary-bg text-sm appearance-none select-carat">
                                <option value="">{{ trans('placeholder.bathrooms') }}</option>
                                @foreach($bathroomsRange as $bathroomCount)
                                    <option value="{{ $bathroomCount }}">{{ trans('label.min') }} {{ $bathroomCount }}+</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    @if(isset($searchControls->min_price) && $searchControls->min_price)
                        <div class="sale_price_filter">
                            @include(themeViewPath('frontend.components.selects.price-select-home'), [
                                'mode' => 'sale',
                                'defaultRentalFrequency' => $defaultRentalFrequency,
                                'defaultCurrency' => $defaultCurrency,
                                'id' => 'sale_min_price',
                                'name' => 'sale_min_price',
                                'prices' => $searchPriceRanges['sale'],
                                'initialOption' => trans('label.min_price'),
                                'currentValue' => null,
                            ])
                        </div>
                    @endif
                    @if(isset($searchControls->max_price) && $searchControls->max_price)
                        <div class="sale_price_filter">
                            @include(themeViewPath('frontend.components.selects.price-select-home'), [
                                'mode' => 'sale',
                                'defaultRentalFrequency' => $defaultRentalFrequency,
                                'defaultCurrency' => $defaultCurrency,
                                'id' => 'sale_max_price',
                                'name' => 'sale_max_price',
                                'prices' => $searchPriceRanges['sale'],
                                'initialOption' => trans('label.max_price'),
                                'currentValue' => null,
                            ])
                        </div>
                    @endif

                    @if(isset($searchControls->min_price) && $searchControls->min_price)
                        <div class="rental_price_filter hidden">
                            @include(themeViewPath('frontend.components.selects.price-select-home'), [
                                'mode' => 'rental',
                                'defaultRentalFrequency' => $defaultRentalFrequency,
                                'defaultCurrency' => $defaultCurrency,
                                'id' => 'rental_min_price',
                                'name' => 'rental_min_price',
                                'prices' => $searchPriceRanges['rental'],
                                'initialOption' => trans('label.min_price'),
                                'currentValue' => null,
                            ])
                        </div>
                    @endif

                    @if(isset($searchControls->max_price) && $searchControls->max_price)
                        <div class="rental_price_filter hidden">
                            @include(themeViewPath('frontend.components.selects.price-select-home'), [
                                'mode' => 'rental',
                                'defaultRentalFrequency' => $defaultRentalFrequency,
                                'defaultCurrency' => $defaultCurrency,
                                'id' => 'rental_max_price',
                                'name' => 'rental_max_price',
                                'prices' => $searchPriceRanges['rental'],
                                'initialOption' => trans('label.max_price'),
                                'currentValue' => null,
                            ])
                        </div>
                    @endif

                    @if(isset($searchControls->status) && $searchControls->status)
                        <div>
                            <select id="status" name="status"
                                    class="form-control w-full py-1 md:py-3 focus:outline-none text-gray-300 border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 tertiary-bg text-sm appearance-none select-carat">
                                <option value="">Status</option>
                                @foreach(Property::searchableTenderStatusList() as $tenderStatus)
                                    <option value="{{ $tenderStatus }}">{{ trans('property_status.'.strtolower($tenderStatus)) }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <input type="hidden" name="locale" value="{{ app()->getLocale() }}">
                    @if(hasFeature(TenantFeature::FEATURE_DEVELOPMENT_SEARCH_ON_HOMEPAGE))
                        <input type="hidden" name="is_development" value="1">
                    @endif

                </div>

                @if($searchableFeatures->count() > 0)
                    <div class="pt-4">
                        @include(themeViewPath('frontend.components.searchable-features'), [
                            'gridClasses' => 'grid-col-1 md:grid-cols-4 gap-2',
                        ])
                    </div>
                @endif

            </div>

        </div>
    </form>
</div>
