@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="meet-the-team-hero" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('team.hero') }}">
        <div class="py-40">
            <div class="container mx-auto px-8 xl:px-0">
                <h1 class="text-white text-5xl font-medium mx-auto py-6 secondary-header-text">{{ trans('header.meet_the_team') }}</h1>
            </div>
        </div>
    </section>

    <section id="meet-the-team" class="px-8">
        <div class="py-8">
            @php
                $members = new \Illuminate\Support\Collection;

                foreach ($departments as $department) {
                    if ($department->members_count) {
                        $members->push($department->members);
                    }
                }

                $members = $members->flatten();
            @endphp

            <div class="grid sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-2">
                @foreach ($members as $member)
                    @include(themeViewPath('frontend.components.cards.team-member'), ['teamMember' => $member])
                @endforeach
            </div>
        </div>
    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
