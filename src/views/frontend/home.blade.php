@extends('layouts.app')

@section('content')

    @push('open-graph-tags')
        @include(themeViewPath('frontend.components.home-open-graph'))
    @endpush

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    {{-- Primary hero element --}}
    @include(themeViewPath('frontend.components.hero.hero-media'))

    {{-- Featured properties band --}}
    @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.featured_properties'), 'properties' => $featuredProperties])

    {{-- Popular searches band --}}
    {{-- @include(themeViewPath('frontend.components.popular-searches')) --}}

    {{-- About us band --}}
    @include(themeViewPath('frontend.components.about'))

    {{-- Valuation band --}}
    @include(themeViewPath('frontend.components.valuation'))

    {{-- Latest Videos band --}}
    @include(themeViewPath('frontend.components.latest-videos'))

    {{-- Testimonials band --}}
    @include(themeViewPath('frontend.components.testimonials'))

    {{-- Latest news band --}}
    @include(themeViewPath('frontend.components.latest-news'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
