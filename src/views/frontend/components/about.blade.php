<section id="about-alternative" class="bg-white px-8 py-12 xl:px-0 text-black">
    <div class="container mx-auto xl:py-12">

        @if(translatableContent('home', 'about-icon'))
            <div class="mb-4">
                <img class="mx-auto h-17 w-auto object-contain" src="{{ assetPath(translatableContent('home', 'about-icon')) }}">
            </div>
        @endif

        <div class="grid grid-cols-8 gap-12">
            <div class="col-span-1 hidden xl:block">&nbsp;</div>

            <div class="col-span-6 text-black mb-8">
                @php
                    $aboutTitleSegments = [];
                    $segments = explode(' ', translatableContent('home', 'about-title'));
                            
                    foreach ($segments as $index => $segment) {
                        if ($index !== count($segments) - 1) {
                            $aboutTitleSegments[] = '<span class="header-text text-3xl sm:text-5xl inline-block">' . $segment . '</span>';
                        } else {
                            $aboutTitleSegments[] = '<span class="bg-primary px-3 py-2 text-white font-bold text-3xl sm:text-5xl inline-block">' . $segment . '</span>';
                        }
                    }
    
                    $aboutTitle = implode(' ', $aboutTitleSegments);
                @endphp
    
                <h2 class="xl:text-5xl text-black header-text text-left uppercase mb-2">{!! $aboutTitle !!}</h2>
                <p class="font-medium">{!! translatableContent('about', 'about-subtitle') !!}</p>
            </div>

            <div class="col-span-1 hidden xl:block">&nbsp;</div>
        </div>
        
        <div class="grid grid-cols-1 lg:grid-cols-8 text-black gap-12">
            <div class="col-span-1 hidden xl:block">&nbsp;</div>

            <div class="col-span-8 xl:col-span-4">
                <div class="text-left home-about-text-container">
                    <p class="text-black text-base font-light" style="color: #696868;">{!! translatableContent('home', 'about-text') !!}</p>
                </div>
            </div>

            @if (hasFeature(\App\Models\TenantFeature::FEATURE_CTA_BUTTONS))
                <div class="col-span-8 sm:col-span-4 xl:col-span-2">
                    @foreach ($ctaButtons as $ctaButton)
                        <div class="grid grid-cols-3 mb-4">
                            <div class="col-span-2 tertiary-bg p-6">
                                <p class="uppercase text-white">{{ $ctaButton->label }}</p>
                            </div>
                            <div class="col-span-1 tertiary-bg text-center w-full h-full" style="display: table;">
                                <a href="{{ $ctaButton->url }}" class="uppercase text-sm block cta-button font-bold" style="display: table-cell; vertical-align: middle;">{{ $ctaButton->button_text }}</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            <div class="col-span-1 hidden xl:block">&nbsp;</div>
        </div>

    </div>
</section>
