@extends('layouts.app')

@section('content')

    @include(themeViewPath('frontend.components.header'))

    <section id="contact" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('contact.hero') }}">
        <div class="py-40">
            <div class="container mx-auto px-8 xl:px-0">
                <h1 class="text-white text-5xl font-medium mx-auto py-6 secondary-header-text">{!! dynamicContent($pageContents, 'contact-title') !!}</h1>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.forms.contact-form'))

    <div>
        @include(themeViewPath('frontend.components.contact-branches'))
    </div>

    @include(themeViewPath('frontend.components.valuation'))

    @include(themeViewPath('frontend.components.footer'))

@endsection
