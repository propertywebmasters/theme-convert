@php
// if the slide is empty then we will inherit from theme default
if($slides->count() === 0) {
    $imagePath = \App\Support\ThemeManager::backgroundCSS('home.hero', tenantManager()->getTenant(), true);
    $slide = new \App\Models\Slide;
    $slide->image = $imagePath;
    $slide->title = optional(app()->make(\App\Services\Content\ContentBlockService::class)->getByIdentifier('home', 'hero-slogan', tenantManager()->getTenant(), app()->getLocale()))->content;
    $slides = collect()->push($slide);
}

$slideSecondaryTitleEnabled = hasFeature(\App\Models\TenantFeature::FEATURE_SLIDE_SECONDARY_TITLE);
$slideTertiaryTitleEnabled = hasFeature(\App\Models\TenantFeature::FEATURE_SLIDE_TERTIARY_TITLE);
@endphp

{{-- loop this --}}
<div id="home-slides" data-speed="12000">
    @php $i = 1; @endphp
    @foreach ($slides as $slide)
        @php
            $displayClass = $i === 1 ? '' : ' hidden';
            $active = $i === 1 ? 'active' : '';
            $tag = $i === 1 ? 'h1' : 'h2';
        @endphp
        <div class="slide slide-{{$i}} {{ $active }} {{ $displayClass }}" data-slide-number="{{ $i }}">
            <div class="relative center-cover-bg h-screen bg-lazy-load" data-style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ assetPath($slide->image) }}');">
                <div class="absolute bottom-1/2 pb-3 sm:pb-4 xl:pb-16 slide-title-container container left-1/2 transform -translate-x-1/2">
                    @php
                        $segments = explode(' ', $slide->title);

                        if (count($segments) > 2) {
                            $primaryTitleSegments = [];
                            
                            foreach ($segments as $index => $segment) {
                                if ($index === 0 || $index === count($segments) - 1) {
                                    continue;
                                }

                                $primaryTitleSegments[] = $segment;
                            }

                            $primaryTitle = $segments[0] . ' <span class="bg-primary text-white text-lg sm:text-2xl font-bold py-2 px-3">' . implode(' ', $primaryTitleSegments) . '</span> ' . $segments[count($segments) - 1];
                        } else if (count($segments) === 2) {
                            $primaryTitle = $segments[0] . ' <span class="bg-primary text-white text-lg sm:text-2xl font-bold py-2 px-3">' . $segments[1] . '</span>';
                        } else {
                            $primaryTitle = $segments[0];
                        }
                    @endphp

                    <{{$tag}} class="block text-white pb-3 sm:pb-6 text-lg sm:text-2xl text-left drop-shadow-lg header-text lg:w-1/2 font-bold px-8 sm:px-0 md:px-4 lg:px-0">
                        {!! $primaryTitle !!}
                    </{{$tag}}>

                    @if ($slideSecondaryTitleEnabled && $slide->secondary_title)
                        <h1 id="slide-secondary-title" class="text-4xl sm:text-6xl xl:text-7xl tracking-tight text-white secondary-header-text px-8 sm:px-0 md:px-4 lg:px-0 pb-3 sm:pb-0">{!! $slide->secondary_title !!}</h1>
                    @endif

                    @if ($slideTertiaryTitleEnabled && $slide->tertiary_title)
                        <h1 id="slide-tertiary-title" class="text-lg sm:text-2xl tracking-tight text-white font-bold px-8 sm:px-0 md:px-4 lg:px-0 pt-4">{!! $slide->tertiary_title !!}</h1>
                    @endif
                </div>
            </div>
        </div>
        @php $i++; @endphp
    @endforeach
</div>

@if($slides->count() > 1)
<div id="home-slides-controls" class="absolute bottom-24 right-0 w-full text-center">
    @php $i = 1; @endphp
    @foreach ($slides as $slide)
        @php
            $active = $i === 1 ? 'active' : ''
        @endphp
        <span class="{{ $active }} bg-white inline-block rounded-full h-4 w-4 mr-1 cursor-pointer" data-slide-number="{{ $i }}">&nbsp;</span>
        @php $i++; @endphp
    @endforeach
</div>
@endif
