@php
use App\Models\SiteSetting;
use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;

$service = app()->make(\App\Services\SocialAccount\Contracts\SocialAccountServiceInterface::class);
$socialAccounts = $service->get();

$socialCount = 0;
if ($socialAccounts !== null) {
    $networks = collect(\App\Models\SocialAccount::SOCIAL_SYSTEMS)->pluck('name')->toArray();

    foreach ($networks as $network) {
        if ($socialAccounts->$network !== null) {
            $socialCount++;
        }
    }

}

$useSettingsCache = config('platform-cache.site_settings_enabled');

$siteEmail = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key'], $useSettingsCache);
$siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key'], $useSettingsCache);
$siteAddress = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_ADDRESS['key'], $useSettingsCache);
@endphp

@if ($socialCount > 0)
<section id="social-footer" class="text-xs footer-section px-8" style="background-color: var(--ap-footer-bg);">
    <div class="container mx-auto lg:py-6 lg:border-t lg:border-b grid grid-cols-1 xl:grid-cols-3 md:px-4" style="border-color: #696868;">
        <div class="flex flex-col lg:flex-row lg:items-center justify-center xl:justify-start col-span-2 mb-6 lg:mb-8 xl:mb-0 border-b lg:border-0 py-8 lg:py-0">
            <div class="lg:pr-8 mb-4 lg:mb-0">
                <a class="text-white text-sm flex items-center" href="tel:{{$siteTel}}">
                    <img class="svg-inject fill-current primary-text mr-2 h-4 xl:mt-0.5 lg:mb-0" src="{{ themeImage('phone-alt.svg') }}" alt="phone">
                    <span class="text-sm lg:text-xs transition-all inline-block lg:whitespace-nowrap text-left">{{ $siteTel }}</span>
                </a>
            </div>
            <div class="lg:pr-8 mb-4 lg:mb-0">
                <a class="text-white text-sm flex items-center" href="javascript:;">
                    <img class="svg-inject fill-current primary-text mr-2 h-5 xl:mt-0.5 lg:mb-0 text-center" src="{{ themeImage('map-marker-alt.svg') }}" alt="email">
                    <span class="text-sm lg:text-xs transition-all lg:whitespace-nowrap text-left hidden sm:inline-block">{{ $siteAddress }}</span>
                    <span class="text-sm lg:text-xs transition-all lg:whitespace-nowrap text-left inline-block sm:hidden">{{ \Illuminate\Support\Str::limit($siteAddress, 40) }}</span>
                </a>
            </div>
            <div>
                <a class="text-white text-sm flex items-center" href="{{ localeUrl('/contact') }}">
                    <img class="svg-inject fill-current primary-text mr-2 h-3 xl:mt-0.5 lg:mb-0" src="{{ themeImage('email.svg') }}" alt="email">
                    <span class="text-sm lg:text-xs transition-all inline-block lg:whitespace-nowrap text-left">{{ $siteEmail }}</span>
                </a>
            </div>
        </div>

        <div class="flex flex-col xl:flex-row justify-end items-start sm:items-center border-b lg:border-0 pb-6 lg:pb-0 transparent-border">
            <div class="xl:pr-6 mb-4 xl:mb-0 hidden follow-us-on-social-media-text">
                <p class="footer-text">{{ trans('generic.follow_us_on_social_media') }}</p>
            </div>

            <div class="flex items-center">
                @foreach($networks as $network)
                    @if($socialAccounts->$network !== null)
                    @php
                    $height = $network === 'youtube' ? 'h-7' : 'h-5';
                    $width = 'w-7';
                    $marginTop = $network === 'youtube' ? '-mt-1' : ''
                    @endphp
                    <div>
                        <a href="{{ $socialAccounts->$network }}" target="_blank" title="{{ $network }}">
                            <img src="{{ themeImage('icons/social/'.$network.'.svg') }}" class="svg-inject {{ $height }} {{ $width }} {{ $marginTop }} fill-current stroke-current primary-text cursor-pointer mx-1" alt="{{ $network }}" loading="lazy" style="color: var(--ap-footer-link);">
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif
