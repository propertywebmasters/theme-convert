@extends('layouts.app')

@push('open-graph-tags')
    @include(themeViewPath('frontend.components.property.open-graph'))
@endpush

@section('content')

    @include(themeViewPath('frontend.components.header'))

    <section id="property-images">
        @include(themeViewPath('frontend.components.property.images'))
    </section>

    <section class="py-8 lg:mt-20 lg:mb-8">
        <div class="container mx-auto">
            <div class="grid lg:grid-cols-2 gap-12 items-center">
                <div class="hidden lg:block">
                    @if ($property->images->count() > 1)
                        <div class="p-10" style="background-color: #F5F5F5;">
                            <img class="property-features-image object-cover" style="aspect-ratio: 1/1;" src="{{ getPropertyImage($property->images[1]->filepath, 'xl') }}" alt="image">
                        </div>
                    @endif
                </div>

                <div class="px-8 lg:px-0">
                    @include(themeViewPath('frontend.components.property.features'))
                    
                    <div class="mt-4 lg:flex items-center">
                        <div class="mb-4 lg:mb-0">
                            <a href="javascript:;" class="cta p-6 px-12 uppercase font-bold text-sm inline-block lg:block tracking-wide border border-cta enquire-now-btn">{{ trans('button.enquire_now') }}</a>
                        </div>

                        @if(hasFeature(\App\Models\TenantFeature::FEATURE_ARRANGE_VIEWING_SYSTEM))
                            <div class="lg:ml-4">
                                <a href="javascript:;" data-target="book-viewing-modal"
                                class="modal-button inline-block lg:block p-6 px-8 text-sm tracking-wide font-bold text-cta border-cta uppercase border arrange-viewing-btn">
                                    {{ trans('header.arrange_a_viewing') }}
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section style="background-color: #F5F5F5;">
        <div class="container mx-auto py-14 lg:py-20 px-8 lg:px-0">
            @if ($property->descriptions->count() > 0)
                @include(themeViewPath('frontend.components.property.description'))
            @endif

            <div class="flex flex-col lg:flex-row items-start lg:items-center mt-6 lg:mt-8">
                <div>
                    @if (hasFeature(\App\Models\TenantFeature::FEATURE_PDF_BEHIND_FORM))
                        <a data-target="brochure-request-modal" class="text-sm font-bold modal-button py-6 px-12 uppercase border download-pdf-button block lg:inline-block" href="javascript:">
                            {{ trans('generic.download_pdf') }}
                        </a>
                    @else
                        <a class="text-xs lg:text-sm font-bold py-4 px-8 lg:py-6 lg:px-12 uppercase border download-pdf-button transition block lg:inline-block text-center" href="{{ $property->pdf_url }}" target="_blank">
                            {{ trans('generic.download_pdf') }}
                        </a>
                    @endif
                </div>

                @if ((hasFeature(\App\Models\TenantFeature::FEATURE_MORTGAGE_CALCULATOR) && $property->tender_type === \App\Models\Property::TENDER_TYPE_SALE) || (hasFeature(\App\Models\TenantFeature::FEATURE_STAMP_DUTY_CALCULATOR) && $property->tender_type === \App\Models\Property::TENDER_TYPE_SALE))
                    @if(hasFeature(\App\Models\TenantFeature::FEATURE_MORTGAGE_CALCULATOR) && $property->tender_type === \App\Models\Property::TENDER_TYPE_SALE)
                        <div class="sm:ml-8 mt-8 lg:mt-0">
                            <a data-target="mortgage-calculator-modal" class="modal-button text-center text-xs sm:text-sm leading-normal tracking-wide font-semibold text-black uppercase cursor-pointer hover-lighten flex items-center">
                                <span class="mr-2">
                                    <img src="{{ themeImage('calculator.svg') }}" class="svg-inject h-6 text-cta" alt="calculator" loading="lazy">    
                                </span> {{ trans('header.mortgage_calculator') }}
                            </a>
                        </div>
                    @endif
                    @if(hasFeature(\App\Models\TenantFeature::FEATURE_STAMP_DUTY_CALCULATOR) && $property->tender_type === \App\Models\Property::TENDER_TYPE_SALE)
                        <div class="sm:ml-8 mt-4 lg:mt-0">
                            <a data-target="stamp-duty-calculator-modal" class="modal-button text-center text-xs sm:text-sm leading-normal tracking-wide font-semibold text-black uppercase cursor-pointer hover-lighten flex items-center">
                                <span class="mr-2">
                                    <img src="{{ themeImage('calculator.svg') }}" class="svg-inject h-6 text-cta" alt="calculator" loading="lazy">    
                                </span> {{ trans('header.stamp_duty_calculator') }}
                            </a>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </section>

    @if ($property->images->count() >= 5)
        <section>
            <div class="grid lg:grid-cols-3 gap-2" style="background-color: #f5f5f5;">
                <div class="hidden lg:block">
                    <img class="h-full object-cover object-center min-h-100 max-h-100" src="{{ getPropertyImage($property->images[2]->filepath, 'xl') }}" alt="">
                </div>

                @if ($property->video_url)
                    @php
                        $videoUrl = getEmbedUrlForPropertyVideo($property->video_url)
                    @endphp

                    <div class="min-h-80 lg:min-h-unset">
                        <iframe class="h-full w-full min-h-100 max-h-100" src="{{ $videoUrl }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                    </div>
                @else
                    <div class="min-h-80 lg:min-h-unset">
                        <img class="h-full object-cover object-center min-h-100 max-h-100" src="{{ getPropertyImage($property->images[3]->filepath, 'xl') }}" alt="">
                    </div>
                @endif

                <div class="hidden lg:block">
                    <img class="h-full object-cover object-center min-h-100 max-h-100" src="{{ getPropertyImage($property->images[4]->filepath, 'xl') }}" alt="">
                </div>
            </div>
        </section>
    @endif

    <section style="background-color: #F5F5F5;">
        @include(themeViewPath('frontend.components.contact-details'))
    </section>

    @if ($relatedProperties->count() > 0)
        <div style="margin-bottom: 4px;">
            <div class="pt-8 pb-4 lg:py-8">
                <h3 class="text-lg lg:text-2xl uppercase font-bold text-center tracking-widest">{{ trans('header.similar_properties') }}</h3>
            </div>
            @include(themeViewPath('frontend.components.featured-properties'), ['title' => trans('header.similar_properties'), 'properties' => $relatedProperties, 'max' => 3, 'style' => ''])
        </div>
    @endif

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

    {{-- CTA Modals --}}

    @if ($property->video_url !== null)
        @include(themeViewPath('frontend.components.modals.video'))
    @endif
    @if ($property->virtual_tour_url !== null)
        @include(themeViewPath('frontend.components.modals.virtual-tour'))
    @endif
    @if($useMap)
        @include(themeViewPath('frontend.components.modals.location'))
    @endif
    @include(themeViewPath('frontend.components.modals.gallery-overlay'))
    @include(themeViewPath('frontend.components.modals.gallery'), ['focusedImgSize' => 'xl'])
    @include(themeViewPath('frontend.components.modals.request-details'))
    @include(themeViewPath('frontend.components.modals.book-viewing'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.mortgage-calculator'))
    @include(themeViewPath('frontend.components.modals.stamp-duty-calculator'))
    @include(themeViewPath('frontend.components.modals.property-note'))
    @include(themeViewPath('frontend.components.modals.share-this-property'))

    @if (hasFeature(\App\Models\TenantFeature::FEATURE_PDF_BEHIND_FORM))
        @include(themeViewPath('frontend.components.modals.brochure-request'))
    @endif

    @if (!hasFeature(\App\Models\TenantFeature::FEATURE_DOCUMENTS_BY_TYPE))
        @if ($property->documents->count() > 0)
            @include(themeViewPath('frontend.components.modals.documents'))
        @endif
    @else
        @if ($floorplanDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-floorplan-documents-modal', 'documents' => $floorplanDocs])
        @endif

        @if ($epcDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-epc-documents-modal', 'documents' => $epcDocs])
        @endif

        @if ($brochureDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-brochure-documents-modal', 'documents' => $brochureDocs])
        @endif

        @if ($otherDocs->count())
            @include(themeViewPath('frontend.components.modals.documents-by-type'), ['id' => 'property-other-documents-modal', 'documents' => $otherDocs])
        @endif
    @endif

@endsection
