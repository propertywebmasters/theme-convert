@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    @php
        use App\Models\TenantFeature;
        $multiColumn = hasFeature(TenantFeature::FEATURE_GENERIC_CONTACT_FORM);
        $breadcrumb['/'] = trans('header.home');
        $breadcrumb['#'] = trans('testimonials.title');

        $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'))
    @endphp

    <section id="testimonials" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('testimonials.hero') }}">
        <div class="py-40">
            <div class="container mx-auto px-8 xl:px-0">
                <h1 class="text-white text-5xl font-medium mx-auto py-6 secondary-header-text">{{ trans('header.what_our_customers_think') }}</h1>
            </div>
        </div>
    </section>

    <section class="py-8">
        <div class="container px-4 mx-auto">
            @foreach ($testimonials as $testimonial)
                <div class="mb-8 border p-10 py-14">
                    <p class="mb-4 text-sm leading-tight font-light" style="color: #696868;">{{ strip_tags($testimonial->content) }}</p>
                    <p class="primary-text leading-tight font-light">
                        <span class="text-sm">{{ $testimonial->name }} | {{ $testimonial->company_name }}</span>
                    </p>
                </div>
            @endforeach

            <div>
                {!! $testimonials->withQueryString()->links('pagination::convert') !!}
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.valuation'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
