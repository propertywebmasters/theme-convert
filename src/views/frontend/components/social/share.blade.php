@php
    if (isset($property)) {
        $message = 'Take a look at this '.$property->displayName().' - '.url()->current();
        $mailtoLink = 'mailto:?subject='.$property->displayName().'&subject&body='.$message;
    } else {
        $message = 'Take a look at this news article '.url()->current();
        $mailtoLink = 'mailto:?subject='.$article->title.'&subject&body='.$message;
    }
    $twitterUrl = 'https://www.twitter.com/intent/tweet?url='.urlencode(url()->current());
    $facebookUrl = 'https://www.facebook.com/sharer/sharer.php?u='.urlencode(url()->current())
@endphp

<ul class="flex justify-center sm:mt-4">
    <li class="mr-2"><a href="{{ $mailtoLink }}" target="_BLANK"><img class="h-7 sm:h-auto" src="{{ themeImage('icons/mail.png') }}" alt="icon" loading="lazy"></a></li>
    <li class="mr-2"><a href="{{ $facebookUrl }}" target="_BLANK" class="share-on-fb" data-url="{{ url()->current() }}"><img class="h-7 sm:h-auto" src="{{ themeImage('icons/fb.png') }}" alt="icon" loading="lazy"></a></li>
    @if(isset($property))
        <li class="sm:mr-2"><a target="_BLANK" href="https://api.whatsapp.com/send?text={{ urlencode($message) }}"><img class="h-7 sm:h-auto" src="{{ themeImage('icons/wa.png') }}" alt="icon" loading="lazy"></a></li>
        {{-- <li>
            @if (hasFeature(\App\Models\TenantFeature::FEATURE_PDF_BEHIND_FORM))
                <a data-target="brochure-request-modal" class="modal-button" href="javascript:"><img src="{{ themeImage('icons/dl.png') }}" alt="icon"></a>
            @else
                <a href="/property/{{ $property->url_key }}/pdf" target="_blank"><img src="{{ themeImage('icons/dl.png') }}" alt="icon"></a>
            @endif
        </li> --}}
    @elseif(isset($article))
        <li><a target="_BLANK" href="https://api.whatsapp.com/send?text={{ urlencode('Take a look at this news article - '.url()->current()) }}"><img class="h-7 sm:h-auto" src="{{ themeImage('icons/wa.png') }}" alt="icon" loading="lazy"></a></li>
    @endif
</ul>
