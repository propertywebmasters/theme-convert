# Installation
- composer require propertywebmasters/convert-theme

# Publish Assets (Required)
- php artisan vendor:publish --tag=convert-theme-assets

# Publish Blade Files
- php artisan vendor:publish --tag=convert-theme-views
