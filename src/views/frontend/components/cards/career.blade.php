@php
    use Illuminate\Support\Str;$imageClasses = 'min-h-46 max-h-46 sm:min-h-46 sm:max-h-46 md:min-h-58 md:max-h-58 lg:min-h-68 lg:max-h-68 xl:min-h-68 xl:max-h-68';
    $description = strip_tags(optional($data)->description);
    $description = Str::limit($description, 425);

    if (!isset($first)) {
        $first = false;
    }

    if (!isset($last)) {
        $last = false;
    }
@endphp

{{-- <div class="p-4 lg:p-8 w-full bg-white shadow-md @if (isset($css) && $css !== null) {{ $css }} @endif">

    <div onclick="window.location='{{ localeUrl('/careers/'.$career->url_key) }}'" class="overflow-hidden center-cover-bg bg-lazy-load cursor-pointer"><!-- --></div>

    <div class="grid grid-cols-4">
        <div class="col-span-3">
            <a href="{{ localeUrl('/careers/'.$career->url_key) }}" class="line-clamp-1 pb-1 overflow-hidden block text-xl font-medium hover:underline primary-text leading-5 inline" title="{!! optional($data)->title !!}">{!! optional($data)->title !!}</a>
        </div>
        <div>
            <span class="text-xl float-right font-bold">{{ $career->salary }}</span>
        </div>
        <div class="col-span-4">
            <p class="text-sm font-normal text-justify pt-4 block pb-6 h-60 overflow-hidden">{!! $description !!}</p>
        </div>
        <div class="col-span-2 border-t-2">
            <span class="flex-1 text-left text-sm xl:text-base font-bold tracking-wider items-baseline uppercase block pt-6">
                {{ $career->created_at->format('jS F Y') }}
            </span>
        </div>
        <div class="col-span-2 text-right border-t-2 pt-6">
            <a class="text-sm xl:text-base primary-text font-bold uppercase hover:underline" href="{{ localeUrl('/careers/'.$career->url_key) }}" title="{!! optional($data)->title !!}">
                {{ trans('generic.read_more') }} <img class="svg-inject inline-block primary-text stroke-current fill-current h-4 pl-1 -mt-0.5" src="{{ themeImage('icons/right-arrow.svg') }}" alt="{{ trans('generic.read_more') }}">
            </a>
        </div>
    </div>
</div> --}}

<div class="border {{ $last ? '' : 'mb-8' }} career-container {{ $first ? 'active' : '' }}">
    <div class="flex items-center justify-between  p-10">
        <div>
            <h3 class="text-xl font-bold tracking-widest uppercase career-title" style="color: #242424;">{!! optional($data)->title !!}</h3>
            <span class="text-sm tracking-tight font-light primary-text career-date"></span>
        </div>

        <div>
            <a class="expand-button {{ $first ? 'hidden' : '' }}" href="javascript:">
                <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject primary-text fill-current stroke-current h-6" alt="arrow" loading="lazy" style="transform: rotate(90deg);">
            </a>

            <a class="shrink-button {{ $first ? '' : 'hidden' }}" href="javascript:">
                <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-6" alt="arrow" loading="lazy" style="transform: rotate(-90deg);">
            </a>
        </div>
    </div>

    <div class="career-more-info p-10 {{ $first ? '' : 'hidden' }}">
        <div class="pb-8 border-b">
            <h5 class="capitalize text-xl tracking-tight">{{ trans('generic.job_description') }}</h5>
        </div>

        <div class="py-8">
            <p class="text-sm tracking-tight font-light" style="color: #242424;">{!! $description !!}</p>
        </div>

        @if ($career->salary)
            <div class="py-6 border-t">
                <p class="text-lg tracking-tight primary-text">{{ trans('generic.salary') }}: {{ $career->salary }}</p>
            </div>
        @endif

        <div class="p-8 py-10" style="background-color: #F5F5F5;">
            <h3 class="text-xl font-bold tracking-widest uppercase mb-6">{{ trans('generic.apply_to_this_position_below') }}</h3>
            @include(themeViewPath('frontend.forms.career'))
        </div>
    </div>
</div>
