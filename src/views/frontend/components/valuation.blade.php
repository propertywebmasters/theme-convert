<section id="valuation" class="bg-lazy-load center-cover-bg" data-style="{{ backgroundCSSImage('home.valuation') }}">
    <div class="flex items-center justify-center h-full w-full" style="min-height: 75vh;">
        <div class="text-center px-8 lg:px-14 xl:px-20 p-20 text-white w-4/5 lg:w-2/5 xl:w-1/3 flex flex-col justify-center" style="background-color: var(--ap-primary-transparent); aspect-ratio: 12 / 9;">
            <h3 class="text-2xl sm:text-3xl font-bold mb-4 uppercase">{{ trans('button.book_a_valuation') }}</h3>
            <p class="mb-8">
                {{ trans('generic.looking_at_selling_your_property') }}
            </p>

            <a href="{{ localeUrl('/valuation') }}" class="border border-white inline-block py-6 px-12 font-bold uppercase text-sm find-out-more-button sm:w-60 lg:w-80 mx-auto">
                {{ trans('generic.find_out_more') }}
            </a>
        </div>
    </div>
</section>