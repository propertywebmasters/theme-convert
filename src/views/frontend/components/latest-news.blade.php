@if ($articles->count() > 0)
    @php
        $headerText = $header ?? trans('header.latest_news');
        $i = 1
    @endphp

    <section id="latest-news" class="py-12 lg:py-20">
        <div class="container mx-auto">
            <div class="items-baseline pb-12 text-center px-8 lg:px-0">
                <h2 class="text-2xl sm:text-3xl font-bold header-text uppercase text-black mb-2 sm:mb-3">{{ $headerText }}</h2>
                <p>{!! translatableContent('home', 'latest-news-subtitle') !!}</p>
            </div>
            <div class="relative latest-news-slider overflow-hidden">
                <div class="absolute top-1/2 left-0 transform -translate-y-1/2 z-30 flex items-center py-4 md:hidden">
                    <div>
                        <div class="latest-news-slider-swiper-button-nextBtn">
                            <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject fill-current stroke-current h-10" alt="arrow" loading="lazy" style="color: #9D9D9D; transform: rotate(180deg); width: 30px; height: 30px; margin-right: 2.5px;">
                        </div>
                    </div>
                </div>

                <div class="absolute top-1/2 right-0 transform -translate-y-1/2 z-30 flex items-center py-4 md:hidden">
                    <div>
                        <div class="latest-news-slider-swiper-button-prevBtn">
                            <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject fill-current stroke-current h-10" alt="arrow" loading="lazy" style="color: #9D9D9D; width: 30px; height: 30px; margin-left: 2.5px;">
                        </div>
                    </div>
                </div>

                <div class="swiper-wrapper md:grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-5 md:gap-4">
                    @foreach($articles as $article)
                        @include(themeViewPath('frontend.components.cards.article'), ['article' => $article, 'css' => $i == 4 ? 'lg:hidden xl:block' : ''])
                        @php $i++ @endphp
                    @endforeach
                </div>
            </div>
            <div class="text-center pt-8 md:pt-12">
                <a style="border-color: var(--ap-primary);" class="inline-block border primary-text uppercase py-6 px-12 hover-lighten text-sm font-bold view-news-archive-button sm:w-60 lg:w-80" href="{{ localeUrl('/news') }}">
                    {{  trans('header.view_news_archive') }}
                </a>
            </div>
        </div>
    </section>
@endif
