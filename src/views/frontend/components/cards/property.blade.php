@php
    $splitMapMode = $splitMapMode ?? false;

    $class = $data = '';
    if (!isset($extraCss)) {
        $extraCss = '';
    }
    if ($splitMapMode) {
        $class = 'map-listing cursor-pointer';
        $data =  'data-zoom="12" data-latitude="'.$property->location->latitude.'" data-longitude="'.$property->location->longitude.'"';
    } else {
        $data =  'data-gallery_images="'.implode(',', propertyImagesArray($property)).'"';
    }
@endphp

@if($property->tender_status !== \App\Models\Property::TENDER_STATUS_AVAILABLE)
    <div class="absolute tender-status top-2 right-2 primary-bg text-white p-2">{{ $property->tender_status }}</div>
@endif

@php
    $imageUrl = $property->images->first() === null ? '/img/image-coming-soon.jpg' : $property->images->first()->filepath;
    $bgImageClass = 'background-position: center center; background-size: cover; cursor: pointer; background-image: url("'.getPropertyImage($imageUrl, 'xl').'")';
@endphp

<div class="relative swiper-slide min-h-140 max-h-140 overflow-hidden">
    <div class="bg-lazy-load bg-center absolute top-0 left-0 min-h-140 max-h-140 w-full" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}" id="property-{{ $property->uuid }}" onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'">&nbsp;</div>
    
    <div class="absolute px-8 pt-8 pb-0 sm:pb-8 w-full bottom-0">
        <div class="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-1 xl:grid-cols-2">
            <div class="bg-white p-6 relative md:col-span-2 lg:col-span-1 featured-property-content">
                @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
                    @if (user() === null)
                        @php
                            $actionClass = 'modal-button';
                            $dataAttribs = 'data-target="preauth-modal"';
                            $imgClass = '';
                        @endphp
                    @else
                        @php
                            if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                                // this property is in the shortlist
                                $actionClass = 'shortlist-toggle-simple';
                                $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                                $imgClass = 'primary-text fill-current stroke-current';
                            } else {
                                // this property is in the shortlist
                                $actionClass = 'shortlist-toggle-simple';
                                $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                                $imgClass = '';
                            }
                        @endphp
                    @endif
                    <div class="flex items-center absolute top-4 right-4 z-20">
                        <div class="cursor-pointer">
                            <a href="javascript:" class="{{ $actionClass }}" {!! $dataAttribs !!}>
                                <img style="width: 17.5px; height: 17.5px;" loading="lazy" class="svg-inject {{ $imgClass }}" src="{{ themeImage('heart-outline.svg') }}" alt="heart" title="{{ trans('shortlist.save') }}">
                            </a>
                        </div>
                    </div>
                @endif
    
                <div>
                    <span class="block xl:pr-12">
                        <a class="text-3xl font-bold text-black line-clamp-1" href="{{ localeUrl('/property/'.$property->url_key) }}">{!! $property->displayPrice(); !!}</a>
                    </span>
                    <p class="location-name mb-4" title="{{ $property->location->displayAddress() }}">
                        <a class="font-medium line-clamp-1 uppercase text-sm" href="{{ localeUrl('/property/'.$property->url_key) }}">{{ $property->location->displayAddress() }}</a>
                    </p>
    
                    {{-- <a href="{{ localeUrl('/property/'.$property->url_key) }}" class="block text-md font-medium tracking-wide hover:underline h-10 leading-5 overflow-hidden line-clamp-2" title="{{ $property->displayName() }}">{!! $property->displayName() !!}</a> --}}
    
                    @php
                        $description = optional($property->description)->long;
                    @endphp
    
                    @if ($description)
                        <p class="mb-4 text-xs">{!! \Illuminate\Support\Str::limit(strip_tags($description), 100) !!}</p>
                    @endif
    
                    @include(themeViewPath('frontend.components.property.bed-bath-info'))
                </div>
            </div>
        </div>
    </div>    
</div>