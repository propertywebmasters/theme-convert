@php
$bedrooms = $property->bedrooms;
$bathrooms = str_replace('.0', '', $property->bathrooms)
@endphp

<span class="text-xs uppercase tracking-wider primary-text font-bold">
    @if($property->bedrooms > 0)
        {{ $bedrooms }} {{ trans('generic.bed') }}
    @endif
    @if ($property->bathrooms != '0')
        @if($property->bedrooms > 0) | @endif {{ $bathrooms }} {{ trans('generic.bath') }}
    @endif
</span>
