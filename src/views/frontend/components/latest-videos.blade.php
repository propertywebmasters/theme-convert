@if ($videos->count() > 0)
    @php
        $i = 0;
    @endphp
    <section id="latest-videos" class="tertiary-bg">
        <div class="container py-12 lg:py-20 lg:px-4 xl:px-0 mx-auto">
            <div class="items-baseline pb-12 text-center px-8">
                <h2 class="text-2xl sm:text-3xl font-bold header-text uppercase text-white mb-3">{{ trans('header.latest_videos') }}</h2>
                <p class="text-center text-white">{!! translatableContent('home', 'latest-videos-subtitle') !!}</p>
            </div>

            <div class="relative latest-videos-slider overflow-hidden">
                <div class="absolute top-1/2 left-0 transform -translate-y-1/2 z-30 flex items-center py-4 md:hidden">
                    <div>
                        <div class="latest-videos-slider-swiper-button-nextBtn">
                            <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject fill-current stroke-current h-10" alt="arrow" loading="lazy" style="color: #9D9D9D; transform: rotate(180deg); width: 30px; height: 30px; margin-right: 2.5px;">
                        </div>
                    </div>
                </div>
    
                <div class="absolute top-1/2 right-0 transform -translate-y-1/2 z-30 flex items-center py-4 md:hidden">
                    <div>
                        <div class="latest-videos-slider-swiper-button-prevBtn">
                            <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject fill-current stroke-current h-10" alt="arrow" loading="lazy" style="color: #9D9D9D; width: 30px; height: 30px; margin-left: 2.5px;">
                        </div>
                    </div>
                </div>
                
                <div class="swiper-wrapper md:grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-5 md:gap-4">
                    @foreach($videos as $video)
                        @php $i++; @endphp
                        @include(themeViewPath('frontend.components.cards.video'), ['video' => $video, 'css' => $i == 4 ? 'lg:hidden xl:block' : ''])
                    @endforeach
                </div>
            </div>

            <div class="text-center pt-12">
                <a class="inline-block border border-white text-white uppercase py-6 px-12 hover-lighten text-sm font-bold view-video-archive-button sm:w-60 lg:w-80" href="{{ localeUrl('/videos') }}">
                    {{  trans('header.view_video_archive') }}
                </a>
            </div>
        </div>
    </section>
    @php $i++; @endphp
@endif
