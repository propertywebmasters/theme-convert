@php
    use App\Models\PropertyDocument;use App\Models\TenantFeature;$showShare = isset($showShare) ? $showShare : true;
    $imageString = $captionString = '';
    foreach ($property->images as $image) {
        $imageString .= getPropertyImage($image->filepath, 'xl').'|';
        $captionString .= $image->caption.'|';
    }

    $imageString = trim($imageString, '|');
    $captionString = trim($captionString, '|');
    $imageCount = $property->images->count();
    $class = '';

    $floorplanDocs = getDocumentsByType(PropertyDocument::TYPE_FLOORPLAN, $property);
    $epcDocs = getDocumentsByType(PropertyDocument::TYPE_EPC, $property);
    $brochureDocs = getDocumentsByType(PropertyDocument::TYPE_BROCHURE, $property);
    $otherDocs = getDocumentsByType(PropertyDocument::TYPE_OTHER, $property);

    $mainImage = $property->images->first();

    if ($mainImage) {
        $mainImage = getPropertyImage($mainImage->filepath, 'xl');
    }
@endphp

<div class="slider" data-images="{{ $imageString }}" data-captions="{{ $captionString }}">
    <div class="w-full cursor-pointer relative" style="max-height: -webkit-fill-available;">
        <div class="relative">
            <div class="loading-gif absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-30" style="display: none;">
                <img src="{{ themeImage('loader.svg') }}" class="svg-inject text-white stroke-current h-12" alt="loader" loading="lazy">
            </div>
            <img id="main-image" src="{{ $mainImage }}" loading="lazy" class="h-72 sm:h-114 md:h-140 lg:h-full w-full object-cover object-center block start-gallery">
            <div class="absolute top-0 left-0 w-full h-72 sm:h-114 md:h-140 lg:h-full overlay bg-black opacity-30 start-gallery z-20" style="pointer-events: none;"></div>
        </div>

        <div class="lg:absolute bottom-0 left-0 h-1/2 lg:h-unset w-full z-30">
            <div class="lg:grid lg:grid-cols-2 lg:gap-12 relative h-full lg:h-unset">
                <div class="h-full lg:h-unset lg:min-h-104 flex flex-col justify-center" style="background-color: var(--ap-secondary-transparent);">
                    <div class="pb-4 py-14 px-8 lg:p-0 lg:container lg:absolute lg:left-1/2 lg:top-1/2 transform lg:-translate-x-1/2 lg:-translate-y-1/2">
                        <div class="grid lg:grid-cols-2 gap-12 container mx-auto">
                            <div>
                                <div>
                                    <span class="text-lg text-white capitalize text-cta mb-3 block">{!! $property->displayPrice() !!} </span>
                                    <h3 class="text-2xl lg:text-4.5xl text-white mb-2 lg:mb-4 secondary-header-text lg:pr-8">{!! $property->displayName() !!}</h3>
                                    <span class="text-xs lg:text-sm font-medium text-white uppercase block text-cta">{{ $property->location->displayAddress() }}</span>
                                </div>

                                <div class="flex items-center mt-8 mb-12">
                                    @if ($property->isHouse())
                                        @if($property->bedrooms > 0)
                                            <div class="pr-4">
                                                <span class="text-sm text-white">{{ $property->bedrooms }} {{ trans('label.bedrooms') }}</span>
                                            </div>
                                        @endif

                                        @if($property->bathrooms > 0)
                                            <div class="border-l px-4">
                                                <span class="text-sm text-white">{{ str_replace('.0', '', $property->bathrooms) }} {{ trans('label.bathrooms') }}</span>
                                            </div>
                                        @endif
                                    @endif

                                    @if($property->internal_size !== null)
                                        @php $unit = getAreaUnitSymbol($property->area_unit);  @endphp
                                        <div class="border-l px-4">
                                            <span class="text-sm text-white">{{ $property->internal_size }}{!! $unit !!}</span>
                                        </div>
                                    @endif

                                    @if($property->land_size !== null)
                                        @php $unit = getAreaUnitSymbol($property->area_unit); @endphp
                                        <div class="border-l px-4">
                                            <span class="text-sm text-white">{{ $property->land_size }}{!! $unit !!}</span>
                                        </div>
                                    @endif

                                </div>

                                <div class="flex justify-between items-center lg:mr-20 pt-4 border-t">
                                    <div>
                                        <span class="uppercase text-white">
                                            <a class="flex items-center" href="{{ url()->previous() }}"><img src="{{ themeImage('long-arrow-left.svg') }}"
                                                                                                             class="svg-inject text-cta fill-current h-9" alt="arrow"
                                                                                                             loading="lazy"> <span
                                                    class="text-xs ml-2">{{ trans('button.back') }}</span></a>
                                        </span>
                                    </div>

                                    <div class="flex items-center">
                                        @if(hasFeature(TenantFeature::FEATURE_SHORTLIST_SYSTEM))
                                            <div class="flex items-center relative tooltip-trigger">
                                                @if (user() === null)
                                                    @php
                                                        $actionClass = 'modal-button';
                                                        $dataAttribs = 'data-target="preauth-modal"';
                                                        $shortlisted = '';
                                                    @endphp
                                                @else
                                                    @php
                                                        $dataAttribs = 'data-on-class="text-cta fill-current stroke-current" data-property="'.$property->uuid.'"';
                                                        $shortlisted = in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray()) ? 'fill-current' : '';
                                                        $actionClass = 'shortlist-toggle-simple';
                                                    @endphp
                                                @endif

                                                <div
                                                    class="tooltip absolute bg-gray-700 z-30 -bottom-8 left-1/2 transform -translate-x-1/2 rounded-sm opacity-0 transition-all ease-in-out duration-100">
                                                    <span class="text-xs text-white px-4 py-1 block">{{ trans('shortlist.save') }}</span>
                                                </div>

                                                <a class="{{ $actionClass }}" href="javascript:" title="{{ trans('shortlist.save') }}" {!! $dataAttribs !!}>
                                                    <img src="{{ themeImage('heart.svg') }}" class="svg-inject h-4 text-cta stroke-current {{ $shortlisted }}" alt="love"
                                                         loading="lazy">
                                                </a>
                                            </div>
                                        @endif

                                        <div class="flex items-center relative tooltip-trigger ml-4">
                                            <div
                                                class="tooltip absolute bg-gray-700 z-30 -bottom-8 left-1/2 transform -translate-x-1/2 rounded-sm opacity-0 transition-all ease-in-out duration-100">
                                                <span class="text-xs text-white px-4 py-1 block">{{ trans('generic.gallery') }}</span>
                                            </div>

                                            <a href="javascript:" class="start-gallery">
                                                <img src="{{ themeImage('expand.svg') }}" class="svg-inject h-5 text-cta stroke-current" alt="expand" loading="lazy">
                                            </a>
                                        </div>

                                        <div class="flex items-center relative tooltip-trigger ml-4">
                                            <div
                                                class="tooltip absolute bg-gray-700 z-30 -bottom-8 left-1/2 transform -translate-x-1/2 rounded-sm opacity-0 transition-all ease-in-out duration-100">
                                                <span class="text-xs text-white px-4 py-1 block">{{ trans('generic.share') }}</span>
                                            </div>

                                            <a class="modal-button" href="javascript:" data-target="share-this-property-modal">
                                                <img src="{{ themeImage('upload.svg') }}" class="svg-inject h-5 text-cta stroke-current" alt="upload" loading="lazy">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hidden lg:block">&nbsp;</div>
                        </div>
                    </div>
                </div>

                <div class="absolute pl-8 lg:pl-0 w-full -top-16 sm:-top-28 md:-top-44 lg:top-unset right-0 lg:relative image-slider-container z-30">
                    <div class="lg:absolute lg:-bottom-16 w-full z-30">
                        <!-- media toggles -->
                        <div class="mb-2">
                            <ul class="flex justify-end lg:inline-block">
                                @if($useMap && $property->location->latitude !== null && $property->location->longitude !== null)
                                    <li style="background-color: var(--ap-secondary-transparent);" class="inline-block mr-1 md:mr-2 text-xs py-1 px-3 md:py-2 md:px-6 modal-button"
                                        data-target="property-location-modal"><span
                                            class="cursor-pointer uppercase text-white text-xs tracking-wide">{{ trans('header.location') }}</span></li>
                                @endif
                                @if(!hasFeature(TenantFeature::FEATURE_DOCUMENTS_BY_TYPE))
                                    @if ($property->documents->count() > 0)
                                        <li style="background-color: var(--ap-secondary-transparent);"
                                            class="inline-block mr-1 md:mr-2 text-xs py-1 px-3 md:py-2 md:px-6 modal-button" data-target="property-documents-modal"><span
                                                class="cursor-pointer uppercase text-white text-xs tracking-wide">{{ trans('header.documents') }}</span></li>
                                    @endif
                                @else
                                    @include(themeViewPath('frontend.components.property.document-by-type-buttons'))
                                @endif
                                @if ($property->video_url !== null)
                                    <li style="background-color: var(--ap-secondary-transparent);" class="inline-block mr-1 md:mr-2 text-xs py-1 px-3 md:py-2 md:px-6 modal-button"
                                        data-target="property-video-modal"><span
                                            class="cursor-pointer uppercase text-white text-xs tracking-wide">{{ trans('header.video') }}</span></li>
                                @endif
                                @if ($property->virtual_tour_url)
                                    <li style="background-color: var(--ap-secondary-transparent);" class="inline-block mr-1 md:mr-2 text-xs py-1 px-3 md:py-2 md:px-6 modal-button"
                                        data-target="property-virtual-tour-modal"><span
                                            class="cursor-pointer uppercase text-white text-xs tracking-wide">{{ trans('header.virtual_tour') }}</span></li>
                                @endif
                            </ul>
                        </div>

                        <div class="image-slider overflow-hidden w-full relative">
                            <div class="swiper-wrapper items-stretch">
                                @foreach ($property->images->slice(1) as $image)
                                    <div class="swiper-slide" style="height: unset;">
                                        <div class="p-1 lg:p-2 pr-0 lg:pr-0 bg-white h-full">
                                            <div class="relative h-full image-slider-image-container">
                                                <div class="absolute top-0 left-0 bg-black opacity-30 w-full h-full"></div>
                                                <img class="h-full w-full object-cover object-center cursor-pointer" src="{{ getPropertyImage($image->filepath, 'xl') }}">
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                @php
                                    $primaryImage = $property->images->first();
                                @endphp

                                <div class="swiper-slide" style="height: unset;">
                                    <div class="p-1 lg:p-2 bg-white h-full">
                                        <div class="relative h-full image-slider-image-container">
                                            <div class="absolute top-0 left-0 bg-black opacity-30 w-full h-full"></div>
                                            <img class="h-full w-full object-cover object-center cursor-pointer" src="{{ getPropertyImage($primaryImage->filepath, 'xl') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cursor-pointer absolute top-1/2 transform -translate-y-1/2 z-30 pl-2 left-0">
                                <button class="swiper-button swiper-button-nextBtn">
                                    <img src="{{ themeImage('caret-left.svg') }}" class="svg-inject text-white stroke-current h-6" alt="arrow" loading="lazy">
                                </button>
                            </div>

                            <div class="cursor-pointer absolute top-1/2 transform -translate-y-1/2 z-30 pr-2 right-0">
                                <button class="swiper-button swiper-button-prevBtn">
                                    <img src="{{ themeImage('caret-right.svg') }}" class="svg-inject text-white stroke-current h-6" alt="arrow" loading="lazy">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
