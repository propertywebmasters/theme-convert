<div class="w-full bg-white overflow-hidden news-article relative swiper-slide px-8 md:px-0 @if (isset($css) && $css !== null) {{ $css }} @endif">

    <div onclick="window.location='{{ localeUrl('/news/'.$article->url_key) }}'" class="min-h-114 max-h-114 relative center-cover-bg bg-lazy-load cursor-pointer" data-style="background-image: url('{{ assetPath($article->image) }}');">
        <div class="overlay w-full h-full primary-bg opacity-0 absolute left-0 top-0"></div>

        <div class="p-4 lg:px-4 xl:p-8 xl:pb-10 absolute min-h-114 max-h-114 -bottom-40 hover:bottom-0 news-article-content">
            <div class="relative flex items-center h-100">
                <div>
                    <p class="text-center text-white uppercase mb-3 text-xs">{{ $article->created_at->format('jS F Y') }}</p>
                    <p class="font-medium line-clamp-5 text-center text-white">{!! \Illuminate\Support\Str::limit(strip_tags($article->content), 50) !!}</p>
                </div>
                {{-- <div class="flex justify-content-end pt-4 xl:pt-6">
                    <div class="flex-1 text-left text-sm xl:text-base font-bold tracking-wider items-baseline uppercase">
                        {{ $article->created_at->format('jS F Y') }}
                    </div>
                    <div class="flex-1 text-right">
                        <a class="text-sm xl:text-base primary-text font-bold uppercase hover:underline" href="{{ localeUrl('/news/'.$article->url_key) }}" title="{{ $article->title }}">
                            {{ trans('generic.read_more') }} <img class="svg-inject inline-block primary-text stroke-current fill-current h-4 pl-1 -mt-0.5" src="{{ themeImage('icons/right-arrow.svg') }}" alt="{{ trans('generic.read_more') }}">
                        </a>
                    </div>
                </div> --}}

                <div class="absolute bottom-0 left-1/2 transform -translate-x-1/2 border-t w-full text-center pt-10">
                    <a class="text-sm xl:text-base text-white font-bold uppercase" href="{{ localeUrl('/news/'.$article->url_key) }}" title="{{ $article->title }}">
                        {{ trans('generic.read_more') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    
</div>
