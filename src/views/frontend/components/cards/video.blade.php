@php
    $embedUrl = $video->getEmbedUrl();
@endphp

<div class="overflow-hidden w-full min-h-114 max-h-114 relative swiper-slide px-8 md:px-0 @if (isset($css) && $css !== null) {{ $css }} @endif">
    <iframe loading="lazy" class="hover:scale min-h-114 max-h-114 w-full" src="{{ $embedUrl }}" title="Video Player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    <div class="px-4 py-4 lg:px-4 xl:px-8 xl:py-8 text-center absolute left-0 bottom-0 w-full z-30" style="background: rgba(0, 0, 0, 0.5);">
        <div class="py-0">
            <span class="block text-lg font-medium primary-text leading-5 text-white line-clamp-1">{{ $video->title }}</span>
        </div>
    </div>

    {{-- <div class="absolute play-button-container z-30 left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2">
        <div class="play-button border border-white p-5">
            <img style="width: 31px; height: auto;" loading="lazy" class="svg-inject text-white" src="{{ themeImage('play-button.svg') }}" alt="play" title="play">
        </div>
    </div> --}}
</div>
