@php
    if (!isset($home)) {
        $home = false;
    }

    if (!isset($mobile)) {
        $mobile = false;
    }

    if (!isset($refinedSearch)) {
        $refinedSearch = false;
    }

    if (!isset($value)) {
        $value = '';
    }

    if (!isset($additionalClasses)) {
        $additionalClasses = '';
    }

    if (!isset($searchResultsContainer)) {
        $searchResultsContainer = 'desktop-search-results-container';
    }

    if (!isset($placeholder)) {
        $placeholder = trans('placeholder.search_location');
    }
@endphp

@if(isset($searchControls->location) && $searchControls->location)
    @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_LOCATION_DATALIST))
        @if ($mobile)
            <div class="search-filter-mobile">
                @include(themeViewPath('frontend.components.location-datalist-refine-mobile'))
            </div>
        @elseif ($refinedSearch)
            <div class="bg-white p-4 col-span-2 relative bg-gray-100 search-filter">
                <label for="search_location" class="text-xs">{{ trans('label.where') }}</label>
                @include(themeViewPath('frontend.components.location-datalist-refine'))
            </div>
        @else
            @include(themeViewPath('frontend.components.location-datalist'))
        @endif
    @elseif ($refinedSearch)
        <div class="bg-white p-4 col-span-2 relative bg-gray-100 search-filter">
            <label for="search_location" class="text-xs">{{ trans('label.where') }}</label>
            
            @include(themeViewPath('frontend.components.location-search-input'), ['value' => $value, 'additionalClasses' => $additionalClasses, 'searchResultsContainer' => $searchResultsContainer])

            <div id="autocomplete-results" class="click-outside absolute top-0 w-full mt-20 -ml-4 z-30"
                    data-classes="py-1 px-4 border bg-white hover:bg-gray-200 text-gray-800 cursor-pointer">
                <ul class="desktop-search-results-container"></ul>
            </div>
            <input type="hidden" name="location_url" value="">
        </div>
    @elseif ($mobile)
        @include(themeViewPath('frontend.components.location-search-input'), ['value' => $value, 'additionalClasses' => $additionalClasses, 'searchResultsContainer' => $searchResultsContainer])

        <input type="hidden" name="location_url" value="">
    @elseif ($home)
        <div class="col-span-4 md:col-span-2 lg:col-span-3 px-4 py-6 relative tertiary-bg">
            <div class="flex items-center">
                {{-- <div class="col-span-1 tertiary-bg h-full flex items-center pr-4">
                    <p class="text-white">{{ trans('header.location') }}</p>
                </div> --}}
    
                <div style="flex-grow: 1;">
                    {{-- @include(themeViewPath('frontend.components.location-search-input'), ['value' => $value, 'additionalClasses' => $additionalClasses, 'placeholder' => trans('placeholder.type_an_area_or_postcode')]) --}}

                    @include(themeViewPath('frontend.components.location-search-input'), ['value' => $value, 'additionalClasses' => $additionalClasses, 'placeholder' => trans('header.location')])
    
                    <div id="autocomplete-results" class="click-outside absolute top-0 w-full z-20 left-0" style="z-index: 100; margin-top: 4.5rem;"
                            data-classes="py-1 px-4 border bg-white hover:bg-gray-200 text-gray-800 cursor-pointer">
                        <ul class="desktop-search-results-container"></ul>
                    </div>
                    <input type="hidden" name="location_url" value="">
                </div>
            </div>
        </div>
    @else
        <div class="col-span-2 sm:col-span-1 lg:col-span-2 bg-white p-4 relative">
            <label for="search_location" class="text-xs">{{ trans('label.where') }}</label>
            
            @include(themeViewPath('frontend.components.location-search-input'), ['value' => $value, 'additionalClasses' => $additionalClasses, 'placeholder' => $placeholder])

            <div id="autocomplete-results" class="click-outside absolute top-0 w-full mt-20 -ml-4 z-20" style="z-index: 100;"
                    data-classes="py-1 px-4 border bg-white hover:bg-gray-200 text-gray-800 cursor-pointer">
                <ul class="desktop-search-results-container"></ul>
            </div>
            <input type="hidden" name="location_url" value="">
        </div>
    @endif
@endif
