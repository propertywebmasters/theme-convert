@php
    $splitMapMode = $splitMapMode ?? false;

    $class = $data = '';
    if (!isset($extraCss)) {
        $extraCss = '';
    }
    if ($splitMapMode) {
        $class = 'map-listing cursor-pointer';
        $infoWindowHtml = view('frontend.components.info-window', ['property' => $property])->render();
        $data =  'data-identifier="'.$property->uuid.'" data-latitude="'.$property->location->latitude.'" data-longitude="'.$property->location->longitude.'" data-infowindow="'.str_replace("\n", "", htmlentities($infoWindowHtml)).'"';
    } else {
        $images = propertyImagesArray($property);
        $data =  'data-gallery_images="'.implode(',', $images).'"';
    }

    $descriptionSummary = null;
    $propertyDescription = $property->descriptions->where('locale', app()->getLocale())->first();
    if ($propertyDescription === null) {
        $propertyDescription = $property->descriptions->first();
    }

    if ($propertyDescription !== null) {
        $tmpDesc = strip_tags($propertyDescription->long);
        $descriptionSummary = \Illuminate\Support\Str::limit($tmpDesc, 300);
    }
@endphp

<div class="flex flex-wrap mb-8 bg-white border border-gray list-view">
    <div class="w-full md:w-1/2 lg:w-5/12 @if(hasFeature(\App\Models\TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY)) inline-listing-gallery @endif @if($splitMapMode) property-listing-map @endif {{ $class }} {{ $extraCss }}" {!! $data !!}>
        <div class="relative h-full">
            {{-- @if($property->tender_status !== \App\Models\Property::TENDER_STATUS_AVAILABLE)
                <div class="absolute tender-status top-2 right-2 primary-bg text-white p-2 px-3 rounded-3xl">{{ $property->tender_status }}</div>
            @endif --}}

            <div class="listings-slider overflow-x-hidden">
                <div class="absolute bottom-0 right-0 z-30 flex items-center px-5 py-4 secondary-bg">
                    <div>
                        <div class="swiper-button-nextBtn">
                            <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy" style="transform: rotate(180deg); width: 17.5px; height: 17.5px; margin-right: 2.5px;">
                        </div>
                    </div>
        
                    <div class="swiper-pagination"></div>
        
                    <div>
                        <div class="swiper-button-prevBtn">
                            <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy" style="width: 17.5px; height: 17.5px; margin-left: 2.5px;">
                        </div>
                    </div>
                </div>

                <div class="swiper-wrapper">
                    @foreach ($images as $image)
                        @php
                            // $imageUrl = $property->images->first() === null ? '/img/image-coming-soon.jpg' : $property->images->first()->filepath;
                            $bgImageClass = 'min-height: 400px; max-height: 400px; overflow-hidden background-position: center center; background-size: cover; width: 100%; cursor: pointer; background-image: url("'.$image.'")';
                        @endphp

                        <div class="swiper-slide overflow-hidden">
                            <div class="bg-lazy-load hover:scale" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}" id="property-{{ $property->uuid }}" onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'">&nbsp;</div>
                        </div>
                    @endforeach
                </div>

            </div>

            {{-- @if(hasFeature(\App\Models\TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY) && !$splitMapMode && $property->images->count() > 1)
                <div class="inline-gallery-control hidden previous-image p-4 absolute left-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                    <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/left-carat.svg') }}" loading="lazy">
                </div>
                <div class="inline-gallery-control hidden next-image p-4 absolute right-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                    <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/right-carat.svg') }}" loading="lazy">
                </div>
            @endif --}}
        </div>
    </div>

    <div class="w-full md:w-1/2 lg:w-7/12 mb-10 pl-4 md:pl-10 pt-10 pr-4 relative flex items-center">
        @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
            @if (user() === null)
                @php
                $actionClass = 'modal-button';
                $dataAttribs = 'data-target="preauth-modal"';
                $imgClass = '';
                @endphp
            @else
                @php
                    if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = 'primary-text fill-current stroke-current';
                    } else {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = '';
                    }
                @endphp
            @endif

            <div class="absolute right-4 top-4">
                <a class="{{ $actionClass }} cursor-pointer hover-lighten transition-all primary-text" href="javascript:;" title="{{ trans('shortlist.save') }}" {!! $dataAttribs !!}>
                    <img src="{{ themeImage('heart-outline.svg') }}" class="svg-inject w-5 h-auto {{ $imgClass }}" alt="love" loading="lazy">
                </a>
            </div>
        @endif

        <div class="py-4 md:py-0">
            <div>
                {{-- <a href="{{ localeUrl('/property/'.$property->url_key) }}" class="@if(!$splitMapMode) text-2xl block @else text-lg text-primary m-0 p-0 overflow-hidden block line-clamp-1 @endif">
                    {!! $property->displayName() !!}
                </a> --}}

                <div class="xl:pr-12">
                    <a class="text-2xl lg:text-3xl font-bold text-black line-clamp-1" href="{{ localeUrl('/property/'.$property->url_key) }}">{!! $property->displayPrice() !!}</a>
                </div><!-- / -->

                <span class="location-name mb-4 block pt-2">
                    <a class="font-medium line-clamp-1 uppercase text-sm" href="{{ localeUrl('/property/'.$property->url_key) }}">{!! $property->location->displayAddress()  !!}</a>
                </span>
                @php
                    $propertyDescription = $property->descriptions->where('locale', app()->getLocale())->first();
                    if ($propertyDescription === null) {
                        $propertyDescription = $property->descriptions->first();
                    }
                @endphp

                @if ($descriptionSummary !== null && !$splitMapMode)
                    <p class="text-sm leading-normal tracking-tight font-light text-black line-clamp-3 mb-4" style="color: #696868;">
                        {{ $descriptionSummary }}
                    </p>
                @endif
            </div>

            @include(themeViewPath('frontend.components.property.bed-bath-info'))
        </div>
    </div>
</div>
