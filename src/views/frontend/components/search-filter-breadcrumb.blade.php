@php
    use App\Models\TenantFeature;$primaryGridCols = '';
    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));

    $bedroomFilterMode = isset($searchControls->bedrooms) && $searchControls->bedrooms ? 'single' : null;
    $bedroomFilterMode = isset($searchControls->bedrooms_as_range) && $searchControls->bedrooms_as_range ? 'multi' : $bedroomFilterMode;

    // define default visibility for select dropdowns
    $salePriceHidden = $searchRequest->get('type') === 'rental' ? 'hidden' : '';
    $rentalPriceHidden = $searchRequest->get('type') === 'sale' ? 'hidden' : '';
    $currentCurrency = currentCurrency();

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();
    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals
@endphp

<div class="flex flex-col z-10">
    <div class="flex flex-col lg:flex-row w-full lg:justify-between lg:items-center px-0">
        <div class="w-full lg:w-auto self-start sm:self-center flex md:flex-row flex-col md:flex-none sm:flex-wrap justify-between items-center">
            <div class="inline-block lg:w-auto md:w-full text-center sm:text-left">
                <h1 class="text-base lg:text-lg leading-tight text-black-600 text-center text-white">{!! $breadcrumbResultsString  !!} ({{ number_format($properties->total()) }}
                    )</h1>
            </div>
        </div>

        <div class="block lg:hidden">

            <div class="grid grid-cols-3 pt-6 px-8 lg:px-0">

                <div class="col-span-2">

                    <a class="modal-button leading-normal font-medium text-sm hover:underline text-gray-400 mr-2" href="javascript:" data-target="share-this-search-modal">
                        <i class="feather-20 inline-block primary-text" data-feather="mail"></i>
                    </a>

                    @if(hasFeature(TenantFeature::FEATURE_ACCOUNT_SYSTEM))
                        @if (user())
                            <a class="modal-button leading-normal font-medium text-sm hover:underline text-gray-400 mr-2" href="javascript:" data-target="create-alert-modal">
                                <i class="feather-20 inline-block primary-text" data-feather="bell"></i>
                            </a>
                        @else
                            <a class="modal-button leading-normal font-medium text-sm hover:underline text-gray-400 mr-2" href="javascript:" data-target="preauth-modal">
                                <i class="feather-20 inline-block primary-text" data-feather="bell"></i>
                            </a>
                        @endif
                    @endif

                </div>

                <div class="text-right">
                    <select id="sort" name="sort" class=" mt-2 autojump w-full block focus:outline-none text-xs md:text-sm bg-transparent appearance-none select-carat text-white">
                        <option value="">{{ trans('sort.sort_by') }}</option>
                        <option value="{{ getSortByUrl('most-recent') }}"
                                @if($searchRequest->get('sort') === 'most-recent') selected @endif>{{ trans('sort.most_recent') }}</option>
                        <option value="{{ getSortByUrl('price-low') }}" @if($searchRequest->get('sort') === 'price-low') selected @endif>{{ trans('sort.lowest_price') }}</option>
                        <option value="{{ getSortByUrl('price-high') }}"
                                @if($searchRequest->get('sort') === 'price-high') selected @endif>{{ trans('sort.highest_price') }}</option>
                    </select>
                </div>

            </div>

        </div>

        <!-- large and higher -->
        <div class="hidden lg:block">
            <div class="w-full lg:w-auto md:pt-4 lg:pt-0 mt-4 sm:mt-0">
                <form class="listings-search flex items-center" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded">
                    <div class="mr-4">
                        @php
                            $isSplitMapView = false
                        @endphp

                        @if(hasFeature(TenantFeature::FEATURE_LIST_VIEW))
                            @php
                                $isListView = request()->get('list');
                                $isGridView = !request()->get('list') && !request()->get('map');
                                $isSplitMapView = request()->get('map')
                            @endphp

                            <div class="inline-block px-0 sm:w-auto">
                                <a class="@if ($isGridView) primary-text @else text-white @endif leading-normal font-medium text-sm flex items-center" href="{{ gridModeUrl() }}">
                                    {{-- <i class="inline-block @if ($isGridView) primary-text @else text-gray-400 @endif" data-feather="grid"></i> --}}

                                    <span class="text-sm inline-block mr-2"><img src="{{ themeImage('grid.svg') }}" class="svg-inject h-5 inline-block text-white"
                                                                                 alt="grid"></span>
                                    <span class="text-sm capitalize-first-letter">{{ trans('search.grid') }}</span>
                                </a>
                            </div>
                            <div class="inline-block px-0 pl-2 sm:w-auto">
                                <a class="@if ($isListView) primary-text @else text-white @endif leading-normal font-medium text-sm flex items-center" href="{{ listModeUrl() }}">
                                    {{-- <i class="inline-block @if ($isListView) primary-text @else text-gray-400 @endif" data-feather="list"></i> --}}

                                    <span class="text-sm inline-block mr-2"><img src="{{ themeImage('list.svg') }}" class="svg-inject h-5 inline-block text-white"
                                                                                 alt="list"></span>
                                    <span class="text-sm capitalize-first-letter">{{ trans('search.list') }}</span>
                                </a>
                            </div>
                        @endif
                        @if(hasFeature(TenantFeature::FEATURE_SPLIT_MAP_VIEW))
                            <div class="hidden md:inline-block px-0 @if(hasFeature(TenantFeature::FEATURE_LIST_VIEW)) pl-2 @endif sm:w-auto">
                                <a class="@if ($isSplitMapView) primary-text @else text-white @endif leading-normal font-medium text-sm flex items-center"
                                   href="{{ mapModeUrl() }}">
                                    {{-- <i class="inline-block @if ($isSplitMapView) primary-text @else text-gray-400 @endif" data-feather="map-pin"></i> --}}

                                    <span class="text-sm inline-block mr-2"><img src="{{ themeImage('map.svg') }}" class="svg-inject h-5 inline-block text-white" alt="map"></span>
                                    <span class="text-sm capitalize-first-letter">{{ trans('search.map') }}</span>
                                </a>
                            </div>
                        @endif
                    </div>

                    @if ($searchRequest->get('location') !== null)
                        @if (user())
                            <div class="inline-block lg:px-3 lg:w-auto md:w-1/3 w-full">
                                <a class="modal-button leading-normal font-medium text-sm text-white flex items-center" href="javascript:" data-target="create-alert-modal">
                                    {{-- <img src="{{ themeImage('icons/bell.svg') }}" class="svg-inject h-5 inline-block fill-current stroke-current text-white"
                                         alt="alert"> --}}

                                    <span class="text-sm inline-block mr-2"><img src="{{ themeImage('bell.svg') }}" class="svg-inject h-5 inline-block text-white"
                                                                                 alt="bell"></span>
                                    <span class="text-sm capitalize-first-letter">{{ trans('search.create_property_alert') }}</span>
                                </a>
                            </div>
                        @else
                            <div class="inline-block lg:px-3 lg:w-auto md:w-1/3 w-full">
                                <a class="modal-button leading-normal font-medium text-sm text-white flex items-center" href="javascript:" data-target="preauth-modal">
                                    {{-- <img src="{{ themeImage('icons/bell.svg') }}" class="click-this svg-inject h-5 inline-block fill-current stroke-current text-white"
                                         alt="alert"> {{ trans('search.create_property_alert') }} --}}

                                    <span class="text-sm inline-block mr-2"><img src="{{ themeImage('bell.svg') }}" class="svg-inject h-5 inline-block text-white"
                                                                                 alt="bell"></span>
                                    <span class="text-sm capitalize-first-letter">{{ trans('search.create_property_alert') }}</span>
                                </a>
                            </div>
                        @endif
                    @endif

                    @if ($searchRequest->get('location') !== null)
                        <div class="inline-block lg:px-3 w-auto lg:w-auto">
                            @if(user() === null)
                                <a href="javascript:" class="modal-button leading-normal font-medium text-sm text-white flex items-center" data-target="preauth-modal">
                                    <span class="text-sm inline-block mr-2"><img src="{{ themeImage('download.svg') }}" class="svg-inject h-5 inline-block text-white" alt="search"></span>
                                    <span class="text-sm">{{ trans('search.save_search') }}</span>
                                </a>
                            @else
                                @if ($savedSearch === null)
                                    <a class="save-this-search leading-normal font-medium text-sm text-white flex items-center"
                                       href="/account/searches/{{ base64_encode($_SERVER['REQUEST_URI']) }}">
                                        <span class="text-sm inline-block mr-2"><img src="{{ themeImage('download.svg') }}" class="svg-inject h-5 inline-block text-white"
                                                                                     alt="search"></span>
                                        <span class="text-sm">{{ trans('search.save_search') }}</span>
                                    </a>
                                @else
                                    <a class="save-this-search leading-normal font-medium text-sm text-white flex items-center"
                                       href="/account/searches/{{ $savedSearch->uuid }}/delete">
                                        <span class="text-sm inline-block mr-2"><img src="{{ themeImage('download.svg') }}" class="svg-inject h-5 inline-block text-white"
                                                                                     alt="search"></span>
                                        <span class="text-sm capitalize-first-letter">{{ trans('search.saved_search') }}</span>
                                    </a>
                                @endif
                            @endif
                        </div>
                    @endif

                    <div class="inline-block lg:px-3 md:px-0 lg:w-auto w-auto ">
                        <a class="modal-button leading-normal font-medium text-sm hover:underline text-white flex items-center" href="javascript:"
                           data-target="share-this-search-modal">
                            {{-- <i class="inline-block text-white" data-feather="mail"></i> {{ trans('search.share_this_search') }} --}}

                            <span class="text-sm inline-block mr-2"><img src="{{ themeImage('share-outline.svg') }}" class="svg-inject h-5 inline-block text-white"
                                                                         alt="share"></span>
                            <span class="text-sm capitalize-first-letter">{{ trans('search.share_this_search') }}</span>
                        </a>
                    </div>

                    <div class="inline-block px-0 pl-2 w-auto float-right lg:float-none">
                        <select id="sort" name="sort" class="autojump w-full block focus:outline-none leading-normal text-sm bg-transparent pr-8 text-white">
                            <option value="">{{ trans('sort.sort_by') }}</option>
                            <option value="{{ getSortByUrl('most-recent') }}"
                                    @if($searchRequest->get('sort') === 'most-recent') selected @endif>{{ trans('sort.most_recent') }}</option>
                            <option value="{{ getSortByUrl('price-low') }}"
                                    @if($searchRequest->get('sort') === 'price-low') selected @endif>{{ trans('sort.lowest_price') }}</option>
                            <option value="{{ getSortByUrl('price-high') }}"
                                    @if($searchRequest->get('sort') === 'price-high') selected @endif>{{ trans('sort.highest_price') }}</option>
                        </select>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
