@extends('layouts.app')

@push('open-graph-tags')
    @include(themeViewPath('frontend.components.article.open-graph'))
@endpush

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <div class="secondary-bg pt-32 pb-60">

        <div class="container mx-auto transparent pt-7 px-8 lg:px-32 xl:px-64 pb-12 text-center">
            <h1 class="text-white text-4xl font-bold mx-auto py-6">{{ $article->title }}</h1>
            <span class="text-base pb-6 text-white">{{ $article->created_at->format('jS F Y') }}</span>
        </div>
    </div>
    <div class="container mx-auto px-8 lg:px-32 xl:px-64 -mt-60">
        <div class="bg-white">
            <img class="w-full shadow" src="{{ assetPath($article->image) }}" loading="lazy">

            <div class="text-sm py-4">
                <a href="/" class="text-sm primary-text">Home</a> &gt; <a href="{{ localeUrl('/news') }}" class="text-sm primary-text">News</a> &gt; {{ $article->title }}
            </div>

            <div>
                @if ($article->subtitle !== null)
                    <h2 class="text-2xl font-bold mx-auto pt-4">{{ $article->subtitle }}</h2>
                @endif
                <p class="pt-4">{!! $article->content !!}</p>
                <hr class="my-6">
            </div>

            <div class="mx-auto text-center pb-2">
                <span class="block">{{ trans('generic.share_this_article') }}</span>
                <div class="py-4">
                    @include(themeViewPath('frontend.components.social.share'), ['url' => route('news.show', [$article->url_key])])
                </div>
            </div>

        </div>

    </div>

    {{-- <div class="bg-gray-200 pt-6 pb-16">
        <div class="container mx-auto px-4">
            

            <h3 class="text-2xl mx-auto py-6 font-medium">Related News Articles</h3>
            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-8">
                @foreach($articles as $article)
                    @include(themeViewPath('frontend.components.cards.article'), ['article' => $article])
                @endforeach
            </div>

        </div>

    </div> --}}

    @include(themeViewPath('frontend.components.latest-news'), ['header' => trans('header.related_news_articles')])


    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
