@php
    use App\Models\SiteSetting;
    use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
    $siteName = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_NAME['key'], config('platform-cache.site_settings_enabled'))
@endphp

    <section id="super-footer" class="py-6 lg:pt-8 text-center sm:text-left footer-section px-8" style="background-color: var(--ap-footer-bg);">
        <div class="container mx-auto md:px-4 lg:px-0">
            <div class="grid grid-cols-1 md:grid-cols-2">
                <div class="text-left">
                    <span class="text-xs footer-text">&copy; Copyright {{ $siteName }} {{ date('Y') }}, All Rights Reserved.</span>
                </div>
                @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHOW_FOOTER_PW_LINK))
                    <div class="text-left md:text-right pt-2 sm:pt-0">
                        <span class="text-xs pw-copyright footer-text">
                            <a class="hover:underline text-xs" href="{{ config('footer.pw_website_link') }}" target="_blank" title="Property Webmasters" rel="noopener">
                                {{ config('footer.pw_website_link_text') }}
                            </a> by Property Webmasters
                        </span>
                    </div>
                @endif
            </div>
        </div>
    </section>

