@if($testimonials->count() > 0)
    <section id="testimonials" class="bg-whiter py-14 sm:pt-20 sm:pb-30 bg-lazy-load center-cover-bg relative" data-style="{{ backgroundCSSImage('home.testimonials') }}">
        <div class="absolute w-full h-full opacity-50 left-0 top-0" style="background-color: var(--ap-primary-transparent);"></div>

        <div class="sm:mb-10 relative">
            <img id="quote-icon" style="width: 60px; height: auto;" loading="lazy" class="svg-inject text-white mx-auto" src="{{ themeImage('quote-left.svg') }}" alt="quote" title="quote">
        </div>
        <!-- Swiper -->
        <div class="testimonials-slider container mx-auto overflow-hidden relative">
            <div class="swiper-wrapper">
                @foreach ($testimonials as $testimonial)
                    @include(themeViewPath('frontend.components.cards.testimonial'), ['testimonial' => $testimonial])
                @endforeach
            </div>

            <div class="flex items-center justify-center">
                <div>
                    <div class="swiper-button-nextBtn">
                        <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy" style="transform: rotate(180deg); width: 17.5px; height: 17.5px; margin-right: 2.5px;">
                    </div>
                </div>
    
                <div class="swiper-pagination"></div>
    
                <div>
                    <div class="swiper-button-prevBtn">
                        <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy" style="width: 17.5px; height: 17.5px; margin-left: 2.5px;">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
