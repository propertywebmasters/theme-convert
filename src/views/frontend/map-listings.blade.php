@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <div id="search-hero" class="bg-black flex items-center justify-center bg-lazy-load center-cover-bg px-8 lg:px-0" data-style="{{ backgroundCSSImage('listings.search') }}">
        <div class="container pt-8 sm:pt-0 xl:pt-8">
            <p class="uppercase font-bold lg:text-2xl text-white">{{ trans('generic.start') }} <span class="primary-bg text-white uppercase font-bold text-2xl py-1 px-3 inline-block">{{ trans('generic.your_next') }}</span></p>
            <h1 class="text-4xl lg:text-6xl secondary-header-text text-white capitalize mt-2">{{ trans('generic.search_today') }}</h1>
        </div>
    </div>

    @include(themeViewPath('frontend.components.map-not-available'))

    <div class="hidden lg:block">
        <div class="py-6 secondary-bg">
            <div class="container mx-auto">
                <div class="pb-4 lg:pb-10">
                    @include(themeViewPath('frontend.components.search-filter'))
                </div>
                <div class="">
                    @include(themeViewPath('frontend.components.search-filter-breadcrumb'))
                </div>
                @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])
            </div>
        </div>

        @php
            use App\Models\SiteSetting;
            use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
            $siteSettingService = app()->make(SiteSettingServiceInterface::class);
            $polygonService = app()->make(\App\Services\LocationPolygon\Contracts\LocationPolygonServiceInterface::class);
            $defaultLatitude = $siteSettingService->retrieve(SiteSetting::SETTING_DEFAULT_MAP_LATITUDE['key'], config('platform-cache.site_settings_enabled'));
            $defaultLongitude = $siteSettingService->retrieve(SiteSetting::SETTING_DEFAULT_MAP_LONGITUDE['key'], config('platform-cache.site_settings_enabled'));
            $defaultZoom = $siteSettingService->retrieve(SiteSetting::SETTING_DEFAULT_MAP_ZOOM['key'], config('platform-cache.site_settings_enabled'));
            if ($polygonData !== null) {
                $polygonDataJson = $polygonService->convertPolygonDataStructure($polygonData);
            }
        @endphp

        <div class="p-4">
            <div class="grid grid-cols-5 gap-4">
                <div class="col-span-3 relative">
                    <div id="map" class="w-full h-screen bg-gray-100 mx-auto text-center" data-zoom="{{ $defaultZoom }}" data-latitude="{{ $defaultLatitude }}" data-longitude="{{ $defaultLongitude }}" @if(isset($polygonDataJson)) data-polygon='{!! json_encode($polygonDataJson) !!}' @endif>
                    <span class="block pt-64 text-3xl">Loading Map</span>
                    </div>
                </div>
                <div class="col-span-2 relative">
                    <div class="h-screen overflow-y-hidden overflow-y-scroll">
                        @php
                            $gridCss = request()->get('list') == true ? 'grid grid-cols-1 gap-4 mt-8' : 'grid grid-cols-2 gap-4';
                        @endphp
                        @if (request('list'))
                            {{-- @foreach($properties as $property)
                                @include(themeViewPath('frontend.components.cards.property-list-view'), ['property' => $property, 'splitMapMode' => true, 'extraCss' => 'map-list-view'])
                            @endforeach --}}

                            @if ($properties->count() > 0)
                                <div class="{{ $gridCss }}">
                                    @foreach($properties->slice(0, 6) as $i => $property)
                                        @include(themeViewPath('frontend.components.cards.property-list-view'), ['property' => $property, 'splitMapMode' => true, 'extraCss' => 'map-list-view'])

                                        @if ($properties->count() <= 6 && hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === (array_key_last($properties->slice(0, 6)->toArray()) - 1))
                                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                                        @endif
                                    @endforeach
                                </div>
                            @endif

                            @if ($properties->count() >= 6)
                                @if (!hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
                                    @include(themeViewPath('frontend.components.listings.listings-divider'), ['splitMapMode' => true])
                                @endif

                                <div class="container px-8 xl:px-0 mx-auto">
                                    <div class="{{ $gridCss }}">
                                        @foreach($properties->slice(6) as $i => $property)
                                            @include(themeViewPath('frontend.components.cards.property-list-view'), ['property' => $property, 'splitMapMode' => true, 'extraCss' => 'map-list-view'])

                                            @if (hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === array_key_first($properties->slice(6)->toArray()))
                                                @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @else
                            {{-- <div class="grid grid-cols-2 gap-4">
                                @foreach($properties as $property)
                                    @include(themeViewPath('frontend.components.cards.property-split'), ['property' => $property, 'splitMapMode' => true, 'extraCss' => 'map-grid-view'])
                                @endforeach
                            </div> --}}

                            @if ($properties->count() > 0)
                                <div class="{{ $gridCss }}">
                                    @foreach($properties->slice(0, 6) as $i => $property)
                                        @include(themeViewPath('frontend.components.cards.property-split'), ['property' => $property, 'splitMapMode' => true, 'extraCss' => 'map-grid-view'])

                                        @if ($properties->count() <= 6 && hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === (array_key_last($properties->slice(0, 6)->toArray()) - 1))
                                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list'), 'splitMapMode' => true])
                                        @endif
                                    @endforeach
                                </div>
                            @endif

                            @if ($properties->count() >= 6)
                                @if (!hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
                                    @include(themeViewPath('frontend.components.listings.listings-divider'), ['splitMapMode' => true])
                                @endif

                                <div class="container px-8 xl:px-0 mx-auto">
                                    <div class="{{ $gridCss }}">
                                        @foreach($properties->slice(6) as $i => $property)
                                            @include(themeViewPath('frontend.components.cards.property-split'), ['property' => $property, 'splitMapMode' => true, 'extraCss' => 'map-grid-view'])

                                            @if (hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === array_key_first($properties->slice(6)->toArray()))
                                                @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list'), 'splitMapMode' => true])
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endif

                        <div class="my-4">
                            {{ $properties->appends(request()->query())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    @include(themeViewPath('frontend.components.search-page-content'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
