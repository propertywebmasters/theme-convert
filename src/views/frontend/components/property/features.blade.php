<div>
    <h3 class="lg:text-2xl font-bold tracking-wider uppercase mb-4">{{ trans('header.property_features') }}</h3>
    <div>
        @foreach($property->features as $feature)
            @php
            $translation = trans('features.'.strtolower($feature->name));
            $translation = stripos($translation, 'features.') !== false ? $feature->name : $translation
            @endphp
            <div class="flex items-center">
                <span style="width: 0.5rem; height: 0.5rem; border-radius: 0.5rem; background-color: var(--ap-cta-bg);" class="mr-2"></span>
                <span class="py-3 block">{{ $translation }}</span>
            </div>
        @endforeach
    </div>
</div>
