@php
$classes = 'border-l px-4 py-2 w-full block focus:outline-none bg-transparent text-sm appearance-none select-carat';
@endphp

@if (!isset($label))
   @php
       $label = trans('placeholder.please_select');
   @endphp 
@endif
<select id="{{ $id }}" name="{{ $name }}" class="{{ $classes }}">
    <option value="">{{ $label }}</option>
    @foreach($prices as $price)
        @php
            $priceFormat = $defaultCurrency->symbol.number_format($price);
            if ($mode === 'rental') {
                $priceFormat .= ' '.trans('rental_frequency.'.$defaultRentalFrequency);
            }
            $selected = $price != 0 && $price == $currentValue ? ' selected="selected"' : ''
        @endphp
        <option value="{{ $price }}" {{ $selected }}>{!! $priceFormat !!}</option>
    @endforeach
</select>
