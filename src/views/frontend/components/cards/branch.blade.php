<section class="bg-whit mb-12">
    <div class="container mx-auto px-8 xl:px-0">
        <div class="bg-white grid grid-cols-1 lg:grid-cols-5 gap-4 border">
            <div class="mapouter relative text-right w-full h-80 lg:h-full lg:col-span-2">
                <iframe class="w-full h-full" id="gmap_canvas" src="https://maps.google.com/maps?q={{urlencode($branch->displayAddress())}}&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
            <div class="py-14 px-7 lg:px-14 lg:col-span-3">
                <h3 class="text-xl lg:text-xl leading-normal font-bold text-primary mb-9 uppercase">{{ $branch->name }}</h3>
                <div>
                    <div class="flex">
                        <img src="{{ themeImage('phone.svg') }}" class="svg-inject primary-text fill-current h-5" alt="phone" loading="lazy">
                        <a href="tel:{{ $branch->tel }}" class="ml-2 text-base leading-tight tracking-tight inline-block">{{ $branch->tel }}</a>
                    </div>
                    <div class="flex items-baseline py-4">
                        <img src="{{ themeImage('map-marker-alt.svg') }}" class="svg-inject primary-text fill-current h-5" alt="marker" loading="lazy">
                        <address class="ml-2 text-base leading-tight tracking-tight inline-block not-italic text-left">{{ $branch->displayAddress() }}</address>
                    </div>
                    <div class="flex">
                        <img src="{{ themeImage('email2.svg') }}" class="svg-inject primary-text fill-current h-5" alt="marker" loading="lazy">
                        <a href="mailto:{{ $branch->email }}" class="ml-2 text-base leading-tight tracking-tight inline-block">{{ $branch->email }}</a>
                    </div>
                    <a href="mailto:{{ $branch->email }}" class="text-sm text-center tracking-wider font-bold uppercase px-4 sm:px-9 right-1 top-1 transition-all inline-block py-6 mt-9 primary-text border" style="border-color: var(--ap-primary);">
                        {{ trans('contact.contact_branch') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
