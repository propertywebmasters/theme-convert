@php
    $propertyDescription = $property->descriptions->where('locale', app()->getLocale())->first();
    if ($propertyDescription === null) {
        $propertyDescription = $property->descriptions->first();
    }
@endphp

<div>
    <h3 class="lg:text-2xl font-bold pb-4 uppercase">{{ trans('header.property_details') }}</h3>
    <p class="text-sm" style="color: #3F3F3F;">{!! optional($propertyDescription)->long !!}</p>
</div>
