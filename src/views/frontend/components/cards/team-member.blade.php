<div class="team-member w-full bg-white shadow-md relative overflow-hidden @if (isset($css) && $css !== null) {{ $css }} @endif">
    <div class="relative h-112 text-center center-cover-bg bg-lazy-load cursor-pointer {{ $teamMember->description ? 'group' : '' }}" data-style="background-image: url({{ assetPath($teamMember->photo) }})" onclick="window.location=href='/meet-the-team/{{ $teamMember->uuid }}'">
        &nbsp;
    </div>
    <div class="team-member-info-container px-4 py-4 lg:px-4 xl:px-8 xl:py-8 xl:pb-0 absolute left-0 w-full h-full z-20">
        <div class="team-member-info sm:overflow-hidden text-center relative h-full w-full">
            <div class="absolute transform -translate-y-1/2 top-1/3 w-full">
                <a class="block font-medium text-lg text-white tracking-tight mb-2">{{ $teamMember->name }}</a>
                <p class="font-normal block text-white text-xs mb-2 uppercase tracking-widest">{{ $teamMember->role }}</p>
            </div>

            <div class="absolute bottom-0 left-0 w-full">
                @php
                    $url = '/meet-the-team/' . $teamMember->uuid;
                @endphp

                <a href="{{ localeUrl($url) }}" class="uppercase text-sm font-bold text-white border-t border-white py-8 block">
                    {{ trans('generic.more_about') }} {{ $teamMember->first_name }}
                </a>
            </div>

            {{-- @if ($teamMember->linked_in !== null || $teamMember->email !== null || $teamMember->tel !== null)
                <div class="team-member-social-networks mx-auto w-full pt-4 md:pt-0">
                    @if($teamMember->linked_in !== null)
                        <div class="rounded-full inline-block primary-bg p-2">
                            <a href="{{ $teamMember->linked_in }}" target="_blank">
                                <img src="{{ themeImage('icons/social/linkedin.svg') }}" class="svg-inject h-4 w-4 fill-current stroke-current text-white cursor-pointer primary-bg" alt="LinkedIn" loading="lazy">
                            </a>
                        </div>
                    @endif
                    @if($teamMember->email !== null)
                        <div class="rounded-full inline-block primary-bg p-2">
                            <a href="mailto:{{ $teamMember->email }}" target="_blank">
                                <img src="{{ themeImage('icons/social/email.svg') }}" class="svg-inject h-4 w-4 fill-current stroke-current text-white cursor-pointer primary-bg" alt="Email" loading="lazy">
                            </a>
                        </div>
                    @endif
                    @if($teamMember->tel !== null)
                        <div class="rounded-full inline-block primary-bg p-2">
                            <a href="tel:{{ $teamMember->tel }}" target="_blank">
                                <img src="{{ themeImage('icons/social/phone.svg') }}" class="svg-inject h-4 w-4 fill-current stroke-current text-white cursor-pointer primary-bg" alt="Phone" loading="lazy">
                            </a>
                        </div>
                    @endif
                </div>
            @endif --}}
        </div>
    </div>

    <div class="overlay absolute primary-bg opacity-0 w-full h-full z-10 top-0 left-0">&nbsp;</div>
</div>
