@php
    $class = 'bg-white text-black block w-full p-4 text-sm mb-3 focus:outline-none shadow';
    $fallbackQuestions = getRecaptchaFallbackQuestionAnswerSet();
    $randomQuestion = array_rand($fallbackQuestions, 1)
@endphp
<form class="recaptcha" action="{{ localeUrl('/careers/'.$career->url_key) }}" method="post" enctype="multipart/form-data">
    <input type="text" name="name" placeholder="{{ trans('contact.full_name') }}" class="{{ $class }}" value="{{ optional(user())->name() }}" required>

    <div class="grid grid-cols-2 gap-3">
        <div>
            <input type="email" name="email" placeholder="{{ trans('contact.email_address') }}" class="{{ $class }}" value="{{ optional(user())->email }}" required>
        </div>
        <div>
            <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}" value="{{ optional(user())->tel }}" class="{{ $class }}" required>
        </div>
    </div>

    <textarea name="message" id="message" cols="30" rows="10" placeholder="{{ trans('generic.message') }}" class="bg-white shadow w-full p-4 mb-3"></textarea>

    <div class="flex items-stretch shadow custom-file-input cursor-pointer mb-3">
        <div class="flex items-center" style="background-color: #696868;">
            <div class="block text-sm text-white tracking-widest font-bold uppercase px-12 cursor-pointer">{{ trans('career.upload_cv') }}</div>
        </div>
        <div class="p-4 bg-white file-chosen" style="color: #777777; flex-grow: 1;">
            {{ trans('generic.no_file_chosen') }}
        </div>
        <input id="file_input" type="file" name="cv" style="width: 0; height: 0;">
    </div>

    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => $class])

    <div class="text-right mt-4">
        <input type="submit" class="relative cta font-bold py-4 px-12 text-sm uppercase cursor-pointer inline-block " value="{{ trans('career.submit_application') }}">
    </div>
    @csrf
</form>

