<div id="contact-details" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('contact.details') }}">
    <div class="grid grid-cols-1 lg:grid-cols-5 lg:gap-12 container mx-auto py-10 lg:py-20 px-8 lg:px-0">
        <div class="col-span-1 lg:col-span-2">
            <div class="xl:pr-20">
                <h3 class="text-2xl lg:text-4.5xl -tracking-wide mb-4 capitalize-first-letter secondary-header-text">{{ trans('header.enquire_about_this_property') }}</h3>
                <p class="text-xs lg:text-sm" style="color: #696868;">Get in contact with us about this property today and one of our team will be in touch.</p>
            </div>

            <div class="hidden lg:block">
                <div class="flex flex-row lg:flex-col xl:flex-row items-center p-5 pl-0 border-t border-b mt-10 text-left items-center">
                    <div class="rounded-full w-28 h-28 mr-5 lg:mr-0 xl:mr-5 mb-0 lg:mb-3 xl:mb-0 {{ empty(assetPath($contactDetails['profile_picture'])) ? 'hidden' : '' }}" style="border: 6px solid var(--ap-cta-bg);">
                        <img class="w-full h-full rounded-full object-cover object-center" src="{{ assetPath($contactDetails['profile_picture']) }}" alt="">
                    </div>
                
                    <div class="{{ empty(assetPath($contactDetails['profile_picture'])) ? 'text-center w-full' : '' }}">
                        <p class="text-xs font-semibold uppercase text-cta">{{ trans('contact.this_property_is_marketed_by') }}</p>
                        <p class="font-light text-lg">{{ $contactDetails['name'] }}</p>
                    </div>
                </div>
        
                <div class="flex flex-col xl:flex-row xl:items-center py-8">
                    @if (isset($contactDetails['tel']))
                        <div class="mb-4 xl:mb-0">
                            <p>
                                <a class="flex items-center" href="tel:{{ $contactDetails['tel'] }}">
                                    <img src="{{ themeImage('phone.svg') }}" class="svg-inject text-cta fill-current h-5" alt="phone" loading="lazy">
                                    <span class="text-sm ml-2">{{ $contactDetails['tel'] }}</span>
                                </a>
                            </p>
                        </div>
                    @endif
                    @if (isset($contactDetails['email']))
                        <div class="{{ isset($contactDetails['tel']) ? 'xl:ml-8' : '' }}">
                            <p>
                                <a class="flex items-center" href="mailto:{{ $contactDetails['email'] }}?subject={{ 'Enquiry for '.$property->displayName().' - ('.$property->displayReference().')' }}">
                                    <img src="{{ themeImage('email2.svg') }}" class="svg-inject text-cta fill-current h-5" alt="marker" loading="lazy">
                                    <span class="text-sm ml-2">{{ $contactDetails['email'] }}</span>
                                </a>
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="col-span-1 lg:col-span-3 mt-6 lg:mt-0">
            @include(themeViewPath('frontend.forms.request-details-inline'))
        </div>

        <div class="col-span-1">
            <div class="lg:hidden">
                <div class="flex flex-row lg:flex-col xl:flex-row items-center py-5 border-t border-b mt-10 text-left items-center">
                    <div class="rounded-full w-20 h-20 xl:mr-5 mb-0 lg:mb-3 xl:mb-0 {{ empty(assetPath($contactDetails['profile_picture'])) ? 'hidden' : '' }}" style="border: 4px solid var(--ap-cta-bg);">
                        <img class="w-full h-full rounded-full object-cover object-center" src="{{ assetPath($contactDetails['profile_picture']) }}" alt="">
                    </div>
                
                    <div class="ml-4 {{ empty(assetPath($contactDetails['profile_picture'])) ? 'text-center w-full' : '' }}">
                        <p class="text-xs font-semibold uppercase text-cta">{{ trans('contact.this_property_is_marketed_by') }}</p>
                        <p class="font-light text-lg">{{ $contactDetails['name'] }}</p>
                    </div>
                </div>
        
                <div class="pt-8">
                    <div class="mb-4">
                        <p>
                            <a class="flex items-center" href="tel:{{ $contactDetails['tel'] }}">
                                <img src="{{ themeImage('phone.svg') }}" class="svg-inject text-cta fill-current h-5" alt="phone" loading="lazy">
                                <span class="text-sm ml-2">{{ $contactDetails['tel'] }}</span>
                            </a>
                        </p>
                    </div>
                    <div>
                        <p>
                            <a class="flex items-center" href="mailto:{{ $contactDetails['email'] }}?subject={{ 'Enquiry for '.$property->displayName().' - ('.$property->displayReference().')' }}">
                                <img src="{{ themeImage('email2.svg') }}" class="svg-inject text-cta fill-current h-5" alt="marker" loading="lazy">
                                <span class="text-sm ml-2">{{ $contactDetails['email'] }}</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>