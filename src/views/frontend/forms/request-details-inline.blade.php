@php
    $class = 'bg-white text-black placeholder-gray-800 block w-full text-sm mb-2 focus:outline-none p-5';
    $style = 'style="color: #777777;"';
    $fallbackQuestions = getRecaptchaFallbackQuestionAnswerSet();
    $randomQuestion = array_rand($fallbackQuestions, 1);
@endphp
<form class="recaptcha" action="{{ localeUrl('/property/'.$property->url_key.'/enquiry') }}" method="post" enctype="application/x-www-form-urlencoded">
    <input type="text" name="name" placeholder="{{ trans('contact.full_name') }}" class="{{ $class }}" value="{{ optional(user())->name() }}" {!! $style !!} required>
    <input type="email" name="email" placeholder="{{ trans('contact.email_address') }}" class="{{ $class }}" value="{{ optional(user())->email }}" {!! $style !!} required>
    <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}" value="{{ optional(user())->tel }}" class="{{ $class }}" {!! $style !!}>
    <textarea name="comment" placeholder="{{ trans('contact.your_message') }}" class="{{ $class }}" rows="6" {!! $style !!} required></textarea>

    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => $class])

    <p class="text-xs modal-terms" style="color: #696868;">{!! trans('terms.modal_terms')  !!}</p>

    <div class="mx-auto mt-8 sm:flex items-center">
        <div>
            <input type="submit" class="relative cta font-bold py-4 px-6 lg:py-6 lg:px-12 text-sm uppercase cursor-pointer inline-block rounded-none" value="{{ trans('button.enquire_now') }}">
        </div>
        @if (shouldShowWhatsappCta())
            <div class="py-2 sm:px-4">
                <span class="uppercase">{{ trans('generic.or') }}</span>
            </div>

            @include(themeViewPath('frontend.components.whatsapp-cta'), compact('property'))
        @endif
    </div>
    @csrf
</form>
