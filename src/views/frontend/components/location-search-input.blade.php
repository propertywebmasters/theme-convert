
@php
if (!isset($datalist)) {
    $datalist = false;
}

if (!isset($additionalClasses)) {
    $additionalClasses = '';
}

if (!isset($value)) {
    $value = '';
}

if (!isset($searchResultsContainer)) {
    $searchResultsContainer = 'desktop-search-results-container';
}

if (!isset($placeholder)) {
    $placeholder = trans('placeholder.search_location');
}
@endphp


<div class="relative">
@if ($datalist)
    <input class="w-full focus:outline-none location-datalist-input pr-8 {{ $additionalClasses }}"  autocomplete="off" role="combobox" list="" name="location" placeholder="{{ trans('placeholder.search_location') }}" style="text-overflow: ellipsis;" value="{{ $value }}">
@else
    <input id="search_location" type="text" name="location" placeholder="{{ $placeholder }}"
        class="autocomplete-location block w-full focus:outline-none pr-8 rounded-none {{ $additionalClasses }}"
        autocomplete="off" value="{{ $value }}" data-search-results-container="{{ $searchResultsContainer }}" style="text-overflow: ellipsis;"/>
@endif

<div class="clear-search-button absolute top-1/2 right-2 transform -translate-y-1/2 cursor-pointer {{ $value ? '' : 'hidden' }}">
    <i class="w-4 h-4" data-feather="x"></i>
</div>
</div>
