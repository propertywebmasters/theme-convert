<footer>
    <!-- FOOTER -->
    <section id="footer" class="footer-section px-8 sm:pt-12 lg:pb-12" style="background-color: var(--ap-footer-bg);">
        <div>

            <div class="grid grid-cols-1 lg:grid-cols-6 sm:pt-4 lg:gap-8 xl:gap-24 container mx-auto">
                @if(hasFeature(\App\Models\TenantFeature::FEATURE_MAILING_LIST))
                    <div class="sm:mb-8 lg:mb-0 md:px-4 lg:px-0 lg:col-span-2">
                        @include(themeViewPath('frontend.components.newsletter-signup'))
                    </div>
                @endif

                @php
                    // DO NOT REMOVE - TAILWIND NEEDS TO SEE THESE TO GENERATE THEM
                    // xl:grid-cols-2 xl:grid-cols-2 xl:grid-cols-3 xl:grid-cols-4 xl:grid-cols-5
                    // lg:grid-cols-1 lg:grid-cols-2 lg:grid-cols-3 lg:grid-cols-4
                @endphp

                <div class="grid grid-cols-1 lg:grid-cols-{{$footerBlocks->count()}} xl:grid-cols-{{$footerBlocks->count() + 1}} text-left col-span-4 md:px-4 lg:px-0 xl:gap-4">
                    <div class="hidden xl:block">&nbsp;</div>
                    @foreach($footerBlocks as $block)
                        <div class="py-6 lg:py-0 border-t lg:border-0 mobile-footer-dropdown-container {{ $loop->last ? 'border-b' : '' }}">
                            <div class="flex items-center justify-between">
                                <span class="sm:pt-2 xl:pb-1 block font-medium footer-text">{!! localeStringFromCollection($block, null, config('platform-cache.footers_enabled')) !!}</span>

                                <div class="{{ $loop->first ? '' : 'hidden' }} minus-button lg:hidden">
                                    <img loading="lazy" class="svg-inject footer-text h-6" src="{{ themeImage('minus.svg') }}" alt="minus">
                                </div>

                                <div class="{{ $loop->first ? 'hidden' : '' }} plus-button lg:hidden">
                                    <img loading="lazy" class="svg-inject footer-text h-6" src="{{ themeImage('plus.svg') }}" alt="plus">
                                </div>
                            </div>


                            <ul class="text-sm leading-6 font-extralight mobile-footer-dropdown {{ $loop->first ? '' : 'hidden' }} lg:block">
                                @foreach ($block->navigation as $link)
                                    <li><a href="{{ localeUrl($link->url) }}" target="{{ $link->target }}" class="text-sm font-light {{ $link->class }}"
                                           title="View {{ localeStringFromCollection($link, null, config('platform-cache.footers_enabled')) }}">{!! localeStringFromCollection($link, null, config('platform-cache.footers_enabled'))  !!}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.accreditations'))
    @include(themeViewPath('frontend.components.social-footer'))
    @include(themeViewPath('frontend.components.super-footer'))
</footer>
