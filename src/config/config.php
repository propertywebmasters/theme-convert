<?php

$parentTheme = 'evolution';
$theme = 'convert';
$config = resource_path('views/vendor/themes/'.$theme.'/config/config.php');

if (file_exists($config)) {
    $additionalCss = resource_path('views/vendor/themes/'.$theme.'/config/custom.css');
    $additionalJs = resource_path('views/vendor/themes/'.$theme.'/config/custom.js');

    // primary theme
    $viewFilePaths = [
        resource_path('views/vendor/themes/'.$theme.'/*.blade.php'),
        resource_path('views/vendor/themes/'.$theme.'/**/*.blade.php'),
        resource_path('views/vendor/themes/'.$theme.'/**/*.js'),
        resource_path('views/vendor/themes/'.$theme.'/**/*.vue'),
    ];
} else {
    $baseDir = __DIR__;
    $additionalCss =  $baseDir . '/custom.css';
    $additionalJs = $baseDir . '/custom.js';

    // primary theme
    $viewFilePaths = [
        $baseDir . '../../../'.$theme.'/*.blade.php',
        $baseDir . '../../../'.$theme.'/**/*.blade.php',
        $baseDir . '../../../'.$theme.'/**/*.js',
        $baseDir . '../../../'.$theme.'/**/*.vue',
    ];
}

return [

    'colours' => [
        'header-font-family-url' => 'https://fonts.googleapis.com/css2?family=Sen:wght@400;700;800&amp;display=swap',
        'header-font-family' => '"Sen", sans-serif',
        'font-family-url' => 'https://fonts.googleapis.com/css2?family=Sen:wght@400;700;800&amp;display=swap',
        'font-family' => '"Sen", sans-serif',
        'primary' => '#23CCCC',
        'secondary' => '#E2450E',
        'tertiary' => '#E2450E',
        'text_colour' => '#212121',
        'cta-background' => '#E2450E',
        'cta-text' => '#FFFFFF',
        // new nav
        'nav-background-colour' => '#FFFFFF',
        'nav-text-colour' => '#707070',
        // new footer
        'footer-background-color' => '#F9F9F9',
        'footer-text-colour' => '#7B7B7B',
        'footer-link-colour' => '#7B7B7B'
    ],

    'backgrounds' => [
        'home.hero' => [
            'filepath' => 'backgrounds/home-hero.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.3',
            'linear_gradient_opacity_end' => '0.3',
        ],
        'home.about' => [
            'filepath' => 'backgrounds/home-about.jpg',
            'linear_gradient_start' => null,
            'linear_gradient_end' => null,
            'linear_gradient_opacity_start' => null,
            'linear_gradient_opacity_end' => null,
        ],
        'home.valuation' => [
            'filepath' => 'backgrounds/home-valuation.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
        'home.testimonials' => [
            'filepath' => 'backgrounds/home-testimonials.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.6',
            'linear_gradient_opacity_end' => '0.6',
        ],
        'valuation.hero' => [
            'filepath' => 'backgrounds/valuation.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.3',
            'linear_gradient_opacity_end' => '0.3',
        ],
        'instant-valuation.hero' => [
            'filepath' => 'backgrounds/instant-valuation-hero.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.3',
            'linear_gradient_opacity_end' => '0.3',
        ],
        'contact.hero' => [
            'filepath' => 'backgrounds/contact.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
        'contact.details' => [
            'filepath' => 'backgrounds/contact-details.png',
            'linear_gradient_start' => null,
            'linear_gradient_end' => null,
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
        'about.hero' => [
            'filepath' => 'backgrounds/about.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
        'testimonials.hero' => [
            'filepath' => 'backgrounds/testimonials.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
        'careers.hero' => [
            'filepath' => 'backgrounds/careers.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
        'area-guide.hero' => [
            'filepath' => 'backgrounds/area-guide.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
        'team.hero' => [
            'filepath' => 'backgrounds/team.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.15',
            'linear_gradient_opacity_end' => '0.15',
        ],
        'team-member.hero' => [
            'filepath' => 'backgrounds/team-member.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.15',
            'linear_gradient_opacity_end' => '0.15',
        ],
        'developments.hero' => [
            'filepath' => 'backgrounds/developments.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.15',
            'linear_gradient_opacity_end' => '0.15',
        ],
        'listings.search' => [
            'filepath' => 'backgrounds/listings-search.jpg',
            'linear_gradient_start' => '000000',
            'linear_gradient_end' => '000000',
            'linear_gradient_opacity_start' => '0.5',
            'linear_gradient_opacity_end' => '0.5',
        ],
    ],

    'contentKeys' => [
        'about' => [
            'about-title',
            'about-section-1-title',
            'about-section-1-text',
            'about-section-2-title',
            'about-section-2-text',
            'about-quoted-text',
            'about-main-title',
            'about-main-text',
            'about-main-image',
        ],
        'auth-modal' => [
            'modal-title',
            'header-image-1',
            'header-text-1',
            'header-image-2',
            'header-text-2',
            'header-main',
            'header-sub',
        ],
        'contact' => [
            'contact-title',
        ],
        'footer' => [
            'newsletter-subscription-title',
            'newsletter-subscription-subtitle',
        ],
        'forgotten-password-modal' => [
            'modal-title',
            'modal-subtitle',
        ],
        'home' => [
            'hero-slogan',
            'feature-header',
            'feature-button-text',
            'feature-button-url',
            'feature-block-1-title',
            'feature-block-1-text',
            'feature-block-2-title',
            'feature-block-2-text',
            'feature-block-3-title',
            'feature-block-3-text',
            'feature-block-4-title',
            'feature-block-4-text',
            'about-title',
            'about-text',
            'featured-developments-header',
            'latest-news-header',
        ],
        'listings' => [
            'listings-alert-divider-text',
        ],
        'login-complete-modal' => [
            'modal-title',
            'modal-subtitle',
            'modal-content',
        ],
        'login-modal' => [
            'modal-title',
            'modal-subtitle',
            'modal-forgotten-password-text',
        ],
        'navigation' => [
            'navigation-login-register-text',
            'navigation-account-text',
        ],
        'new-password-modal' => [
            'modal-title',
            'modal-subtitle',
        ],
        'property' => [
            'social-share-text',
        ],
        'register-complete-modal' => [
            'modal-title',
            'modal-subtitle',
            'modal-content',
        ],
        'register-modal' => [
            'modal-title',
            'modal-subtitle',
            'modal-content',
        ],
        'reset-code-modal' => [
            'modal-title',
            'modal-subtitle',
            'modal-content',
        ],
        'shortlist' => [
            'unsaved',
            'saved',
        ],
        'valuation' => [
            'valuation-intro-title',
            'valuation-intro-text',
            'valuation-form-title',
            'valuation-form-subtitle',
        ],
    ],
    'additionalContentKeys' => [
        [
            'page' => 'home',
            'identifier' => 'latest-videos-subtitle',
            'type' => 'text',
            'content' => '',
        ],
        [
            'page' => 'home',
            'identifier' => 'latest-news-subtitle',
            'type' => 'text',
            'content' => '',
        ],
    ],
    'additionalCSS' => file_get_contents($additionalCss),
    'additionalJS' => file_get_contents($additionalJs),
    'extended_from' => $parentTheme,
    'view_file_paths' => array_merge(
        $viewFilePaths,
        [
            // default theme fallback
            resource_path('views/frontend/*.blade.php'),
            resource_path('views/frontend/**/*.blade.php'),
            resource_path('views/frontend/**/*.vue'),
            resource_path('views/frontend/*.html'),
        ]
    ),
];
