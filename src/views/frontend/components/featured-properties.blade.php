@php
    $i = 0;

    if (!isset($max)) {
        $max = 5;
    }

    if (!isset($style)) {
        $style = 'margin-top: 4px;';
    }
@endphp
@if ($properties->count() > 0)
    <section id="featured-properties" class="relative overflow-hidden" {!! $style !!}>
        <div class="featured-properties-slider">
            <div class="absolute top-0 right-0 z-30 flex items-center px-5 py-4 secondary-bg">
                <div>
                    <div class="swiper-button-nextBtn">
                        <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy" style="transform: rotate(180deg); width: 17.5px; height: 17.5px; margin-right: 2.5px;">
                    </div>
                </div>
    
                <div class="swiper-pagination"></div>
    
                <div>
                    <div class="swiper-button-prevBtn">
                        <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white fill-current stroke-current h-8" alt="arrow" loading="lazy" style="width: 17.5px; height: 17.5px; margin-left: 2.5px;">
                    </div>
                </div>
            </div>

            <div class="swiper-wrapper">
                @foreach($properties as $property)
                    @if($i >= $max) @break @endif
                    @include(themeViewPath('frontend.components.cards.property'), ['property' => $property])
                    @php $i++ @endphp
                @endforeach
            </div>
        </div>
        
    </section>


@endif
