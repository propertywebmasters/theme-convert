@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="team-member-hero" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('team-member.hero') }}">
        <div class="py-40">
            <div class="container mx-auto px-8 xl:px-0">
                <h1 class="text-white text-5xl font-medium mx-auto py-6 secondary-header-text">{{ trans('header.meet_the_team') }}</h1>
            </div>
        </div>
    </section>

    <section class="py-20">
        <div class="container px-8 xl:px-0 mx-auto">
            <div class="grid grid-cols-1 lg:grid-cols-4 gap-8">
                <div class="col-span-1">
                    @if ($teamMember->photo)
                        <img src="{{ assetPath($teamMember->photo) }}" class="w-full object-cover object-center lg:-mt-32" style="aspect-ratio: 10/15; border: 8px solid #F5F5F5;">
                    @endif
                </div>

                <div class="col-span-1 lg:col-span-3 text-center lg:text-left">
                    <div class="border-b pb-8">
                        <h2 class="text-2xl font-medium" style="color: #242424;">{!!  $teamMember->name  !!}</h2>
                        <span class="block pt-2 text-xs font-medium primary-text uppercase tracking-widest">{!!  $teamMember->role  !!}</span>
                    </div>

                    <p class="pt-8 text-sm mb-12" style="color: #242424;">
                        {!! str_replace('<br />', '<br /><br />', $teamMember->description) !!}
                    </p>

                    <div class="flex items-center justify-center lg:justify-start">
                        <a href="javascript:;" class="scroll-to bg-primary px-8 py-5 uppercase text-white text-sm font-bold border" data-target="contact-form" style="border-color: var(--ap-primary);">
                            {{ trans('generic.contact') }} {{ $teamMember->first_name }}
                        </a>

                        <a href="{{ localeUrl('/meet-the-team') }}" class="px-8 py-5 primary-text border font-bold text-sm uppercase ml-4" style="border-color: var(--ap-primary);">
                            {{ trans('button.back_to_team') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.forms.contact-form'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
