@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="about" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('careers.hero') }}">
        <div class="py-40">
            <div class="container mx-auto px-8 xl:px-0">
                <h1 class="text-white text-5xl font-medium mx-auto py-6 secondary-header-text">{{ trans('header.latest_careers') }}</h1>
            </div>
        </div>
    </section>

    <section id="careers" class="py-14">
        <div class="container px-8 xl:px-0 mx-auto">

            @include('frontend.components.system-notifications', ['customClass' => 'mb-6'])

            <div>
                @foreach($careers as $i => $career)
                    @include(themeViewPath('frontend.components.cards.career'), [
                        'career' => $career,
                        'data' => $service->findByVacancyUuidAndLocale($career->uuid, app()->getLocale()),
                        'first' => $loop->first,
                        'last' => $loop->last,
                    ])
                @endforeach
            </div>


        </div>

        @include(themeViewPath('frontend.components.listings.listings-pagination'), ['data' => $careers])

    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
