<div class="swiper-slide flex-shrink-0">
    <div class="p-6 pt-0 pb-0 text-center relative xl:px-80 lg:px-40 md:px-4 sm:px-2 px-8">

        <p class="text-lg sm:text-2xl leading-tight text-center tracking-tight text-white border-b border-white px-0 py-10 lg:px-10">{!! $testimonial->content !!}</p>
        <div class="w-full py-6">
            <span class="text-sm text-center tracking-tight primary-text inline-block block py-2 text-white uppercase text-white font-bold">
                {{ $testimonial->company_name ? $testimonial->name . ', ' . $testimonial->company_name : $testimonial->name }}
            </span>
        </div>
    </div>
</div>

