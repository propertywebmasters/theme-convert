@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="property-listings">

        <div id="search-hero" class="bg-black flex items-center justify-center bg-lazy-load center-cover-bg px-8 lg:px-0" data-style="{{ backgroundCSSImage('listings.search') }}">
            <div class="container pt-8 sm:pt-0 xl:pt-8">
                <p class="uppercase font-bold lg:text-2xl text-white">{{ trans('generic.start') }} <span class="primary-bg text-white uppercase font-bold text-2xl py-1 px-3 inline-block">{{ trans('generic.your_next') }}</span></p>
                <h1 class="text-4xl lg:text-6xl secondary-header-text text-white capitalize mt-2">{{ trans('generic.search_today') }}</h1>
            </div>
        </div>

        <div class="py-6 secondary-bg">
            <div class="container mx-auto">
                <div class="pb-4 lg:pb-10">
                    @include(themeViewPath('frontend.components.search-filter'))
                </div>
                @include(themeViewPath('frontend.components.search-filter-breadcrumb'))
            </div>
        </div>


        <div class="container px-8 lg:px-0 mx-auto">


            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])

            @php
            $cardViewFile = request()->get('list') == true ? 'property-list' : 'property-alt';
            $gridCss = request()->get('list') == true ? 'grid grid-cols-1 gap-4 mt-11' : 'grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8 my-8'
            @endphp

            @if ($properties->count() > 0)
                <div class="{{ $gridCss }}">
                    @foreach($properties->slice(0, 6) as $i => $property)
                        @php $cardViewFile = $property->is_development ? 'development' : $cardViewFile @endphp
                        @include(themeViewPath('frontend.components.cards.'.$cardViewFile), ['property' => $property])

                        @if ($properties->count() <= 6 && hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === (array_key_last($properties->slice(0, 6)->toArray()) - 1))
                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                        @endif
                    @endforeach
                </div>

            @else
                <div class="bg-gray-100 text-center p-12">
                    <span class="text-2xl block font-medium pb-4 pb-4 mb-4">{{ trans('search.no_properties_found') }}</span>
                    <a class="primary-bg hover-lighten text-white font-bold py-4 md:mt-0 px-8 text-sm uppercase cursor-pointer block lg:inline-block" href="{{ url()->current() }}#listings-search">{{ trans('search.no_results_button') }}</a>
                </div>
            @endif
        </div>

        @if ($properties->count() >= 6)
            @if (!hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
                @include(themeViewPath('frontend.components.listings.listings-divider'))
            @endif

            <div class="container px-8 xl:px-0 mx-auto">
                <div class="{{ $gridCss }}">
                    @foreach($properties->slice(6) as $i => $property)
                        @php $cardViewFile = $property->is_development ? 'development' : $cardViewFile @endphp
                        @include(themeViewPath('frontend.components.cards.'.$cardViewFile), ['property' => $property])

                        @if (hasFeature(\App\Models\TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === array_key_first($properties->slice(6)->toArray()))
                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                        @endif
                    @endforeach
                </div>
            </div>
        @endif

        @include(themeViewPath('frontend.components.listings.listings-pagination'), [
            'data' => $properties,
            'extraClass' => 'mb-8'
        ])
    </section>

    {{-- @include(themeViewPath('frontend.components.search-page-content')) --}}

    {{-- Valuation band --}}
    @include(themeViewPath('frontend.components.valuation'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

    {{-- CTA Models --}}
    @include(themeViewPath('frontend.components.modals.create-alert'))

@endsection
